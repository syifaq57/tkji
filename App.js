import React from 'react';
import 'react-native-gesture-handler';
import Routes from './src/Routes';
// import SheetJS from './src/view/Admin/TesTable';
import codePush from 'react-native-code-push';
import {View, Text} from 'react-native';

const App = () => {
  return <Routes />;
  // return (
  //   <View style={{justifyContent: 'center', alignItems: 'center'}}>
  //     <Text>App Disabled - Contact Developer</Text>
  //   </View>
  // )
};

export default codePush(App);
// export default App;

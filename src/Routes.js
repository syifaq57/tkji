import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import LoginScreen from './view/Login/LoginScreen';

import HomeAdmin from './view/Admin/HomeAdmin';
import SplashScreen from './view/SplashScreen/SplashScreen';
import InputTes from './view/Admin/InputTes';
import DataUser from './view/Admin/DataUser';
import InputUser from './view/Admin/InputUser';
import DataTes from './view/Admin/DataTes';
import DetailTes from './view/Component/DetailTes';
import InputNilaiTes from './view/Admin/InputNilaiTes';
import InputJurusan from './view/Admin/InputJurusan';
import InputKelas from './view/Admin/InputKelas';
import LaporanTes from './view/Admin/LaporanTes';
import InputSiswa from './view/Admin/InputSiswa';

const Stack = createStackNavigator();

const Route = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="LaporanTes"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen name="HomeAdmin" component={HomeAdmin} />
        <Stack.Screen name="InputUser" component={InputUser} />
        <Stack.Screen name="DataTes" component={DataTes} />
        <Stack.Screen name="InputTes" component={InputTes} />
        <Stack.Screen name="DataUser" component={DataUser} />
        <Stack.Screen name="InputJurusan" component={InputJurusan} />
        <Stack.Screen name="InputKelas" component={InputKelas} />
        <Stack.Screen name="DetailTes" component={DetailTes} />
        <Stack.Screen name="InputNilaiTes" component={InputNilaiTes} />
        <Stack.Screen name="LaporanTes" component={LaporanTes} />
        <Stack.Screen name="InputSiswa" component={InputSiswa} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Route;

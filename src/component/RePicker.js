/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text} from 'react-native';
// import {Picker} from '@react-native-picker/picker';
import {Picker} from 'native-base';
import colors from '../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const RePicker = (props) => {
  const labelStyle = {
    position: 'absolute',
    top: hp(0.1),
    fontSize: wp(2.8),
    color: colors.primarydark,
    fontFamily: 'Poppins-Regular',
    marginLeft: wp(-0.5),
  };


  const getItems = () => {
    return props.items && props.items.length > 0 ? props.items : []
  };
   return (
    <View
      style={{
        borderBottomWidth: 2,
        borderColor: colors.gray02,
      }}>
      <Picker
        selectedValue={props.selectedValue}
        mode="dropdown"
        style={{
          minHeight: hp(7.8),
          color:
            props.selectedValue && props.selectedValue.length > 0
              ? colors.gray08
              : colors.gray11,
          marginTop: wp(1.5),
          marginBottom: wp(-1.5),
          // position: 'absolute',
          marginLeft: wp(-1),
        }}
        onValueChange={(itemValue, itemIndex) =>
          props.onValueChange(itemValue)
        }>
        <Picker.Item label={props.label} value="" />
         {getItems().map((item, index) => (
          <Picker.Item key={index} label={item.label} value={item.value} />
        ))}
      </Picker>
      {(props.selectedValue && props.selectedValue.length) ||
      props.noAnimation > 0 ? (
        <Text style={[labelStyle]}> {props.label} </Text>
      ) : null}
    </View>
  );
};

export default RePicker;

/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { View, Text, Image} from 'react-native';
import Modal from 'react-native-modal';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const ReLoadingModal = (props) => {
  return (
    <Modal
      style={{ alignItems: 'center' }}
      backdropOpacity={0.2}
      animationIn={'fadeIn'}
      animationOut={'fadeOut'}
      useNativeDriver={true}
      isVisible={props.visible}>
      <View
        style={{
          backgroundColor: 'white',
          borderRadius: wp('2%'),
          padding: hp('1%'),
          width: wp('50%'),
        }}>
        <View
          style={{
            paddingBottom: hp('1%'),
            borderBottomWidth: 0.5,
            borderColor: 'gray',
            alignItems: 'center',
          }}>
          <Text style={{ fontSize: wp('3%'), fontWeight: 'bold' }}>
            {props.title ? props.title : 'Loading ..'}
          </Text>
        </View>
        <View style={{ paddingVertical: hp('2%'), alignItems: 'center' }}>
          <Image
            style={{
              width: hp('10%'),
              height: hp('10%'),
              resizeMode: 'contain'
            }}
            source={require('../res/image/running.gif')}
          />
          {/* <ActivityIndicator size={'large'} color="black" /> */}
        </View>
      </View>
    </Modal>
  );
};

export default ReLoadingModal;

/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, Text, TouchableOpacity, TextInput} from 'react-native';
import {Picker} from 'native-base';
import colors from '../res/colors';
import Modal from 'react-native-modal';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ScrollView, State} from 'react-native-gesture-handler';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const RePickerCustom = (props) => {
  const [selectedValue, setSelectedValue] = useState(props.value || {});
  const [showList, setShowList] = useState(false);
  const [search, setSearch] = useState('');

  const toggleShowList = () => {
    setShowList(!showList);
  };

  // tipe data array
  const nameValue = props.displayValue || 'label';
  const prefixValue = props.prefixValue;

  const getList = () => {
    let result = props.data;

    result = result?.filter((item) => {
      if (nameValue) {
        return (
          item[nameValue].toLowerCase().includes(search.toLowerCase()) ||
          item[prefixValue]?.includes(search)
        );
      }
    });
    return result;
  };

  return (
    <View
      pointerEvents={props.disabled ? 'none' : 'auto'}
      style={{
        borderBottomWidth: 2,
        borderColor: colors.gray02,
      }}>
      <Text
        style={{
          position: 'absolute',
          top: hp(0.1),
          marginLeft: wp(0.5),
          fontSize: wp(2.8),
          fontFamily: 'Poppins-Regular',
          color: colors.primarydark,
        }}>
        {props.label}
      </Text>
      <TouchableOpacity
        style={{
          minHeight: hp(7.6),
          flexDirection: 'row',
          alignItems: 'center',
        }}
        onPress={() => toggleShowList()}>
        {Object.keys(selectedValue).length > 0 ? (
          <View
            style={{
              paddingLeft: wp(0.5),
              marginTop: hp(2),
              marginBottom: hp(-0.6),
            }}>
            <Text
              style={{
                fontSize: wp(4),
                fontFamily: 'Poppins-Regular',
                color: props.valueColor ? props.valueColor : colors.gray08,
              }}>
              {selectedValue[nameValue]}
            </Text>
          </View>
        ) : (
          <View
            style={{
              paddingLeft: wp(1),
              marginTop: hp(2),
              marginBottom: hp(-0.6),
            }}>
            <Text
              style={{
                fontSize: wp(4),
                fontFamily: 'Poppins-Regular',
                color: colors.gray11,
              }}>
              {props.label}
            </Text>
          </View>
        )}
        <View style={{position: 'absolute', top: hp(2.8), right: wp(3.5)}}>
          <FontAwesome name="caret-down" color={colors.gray02} size={wp(3.9)} />
        </View>
      </TouchableOpacity>

      <Modal
        isVisible={showList}
        onBackdropPress={() => toggleShowList()}
        backdropOpacity={0.5}
        animationOut={'slideOutUp'}
        animationIn={'slideInDown'}
        animationInTiming={500}>
        <View
          style={{
            // position: 'absolute',
            backgroundColor: 'white',
            paddingBottom: wp(1.2),
            borderRadius: wp(1),
            height: props.data && props.data.length > 5 ? hp(60) : hp(50),
          }}>
          <View
            style={{
              padding: wp(2.5),
              justifyContent: 'center',
              borderBottomWidth: 1,
              borderTopLeftRadius: wp(1),
              borderTopRightRadius: wp(1),
              backgroundColor: colors.primarydark,
              borderColor: colors.gray02,
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: hp(2.8),
                fontFamily: 'JosefinSans-Bold',
                color: colors.white,
              }}>
              {props.label?.toUpperCase()}
            </Text>
          </View>
          <View
            style={{
              paddingVertical: wp(2),
              paddingHorizontal: wp(3),
              flexDirection: 'row',
            }}>
            <FontAwesome
              name="search"
              color={colors.gray10}
              size={wp(4.2)}
              style={{
                textAlignVertical: 'center',
                marginTop: hp(0.2),
                marginRight: wp(2),
              }}
            />
            <TextInput
              style={{padding: 0}}
              value={search}
              onChangeText={(text) => setSearch(text)}
              placeholder="Search by npm / nama"
            />
          </View>
          <ScrollView style={{}}>
            {getList().map((data, index) => (
              <TouchableOpacity
                onPress={() => {
                  setSelectedValue(data);
                  props.onValueChange(data);
                  toggleShowList();
                }}
                key={index}
                style={{
                  backgroundColor:
                    index % 2 === 0 ? colors.gray01 : colors.white,
                  paddingVertical: hp(1.7),
                  paddingHorizontal: wp(3),
                  flexDirection: 'row',
                }}>
                <View style={{flexDirection: 'row'}}>
                  <FontAwesome
                    name="chevron-right"
                    color={colors.gray10}
                    size={wp(3)}
                    style={{
                      marginTop: hp(0.6),
                      marginRight: wp(1.2),
                      textAlignVertical: 'center',
                    }}
                  />
                  {prefixValue ? (
                    <Text
                      style={{
                        fontSize: hp(2.8),
                        fontFamily: 'JosefinSans-Regular',
                        color: colors.gray10,
                      }}>
                      {`${data[prefixValue]} - `}
                    </Text>
                  ) : null}

                  <Text
                    style={{
                      fontSize: hp(2.8),
                      fontFamily: 'JosefinSans-Regular',
                      color: colors.gray10,
                    }}>
                    {data[nameValue]}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>
      </Modal>
    </View>
  );
};

export default RePickerCustom;

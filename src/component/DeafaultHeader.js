/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import {Text, Header} from 'native-base';
import colors from '../res/colors/index';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';

const DeafaultHeader = (props) => {
  const navigation = useNavigation();
  return (
    <Header
      transparent
      style={
        {
          // backgroundColor: colors.white,
        }
      }>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          flexDirection: 'row',
          // paddingLeft: wp(5),
          // paddingRight: wp(3.5),
          marginHorizontal: wp(3),
          justifyContent: 'center',
          // borderWidth: 1
          // shadow
        }}>
        {props.backButton ? (
          <View style={{position: 'absolute', zIndex: 1000, left: 0}}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}>
              <FontAweSome
                name="arrow-left"
                color={colors.primarydark}
                size={hp(3.2)}
              />
            </TouchableOpacity>
          </View>
        ) : null}
        {props.menuBar ? (
          <View style={{position: 'absolute', zIndex: 1000, left: 0}}>
            <TouchableOpacity
              style={{
                padding: wp(1),
                height: wp(7),
                width: wp(7),
                borderRadius: wp(6),
                backgroundColor: colors.primarydark,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={props.onMenuBarPress}>
              <FontAweSome name="navicon" color={colors.white} size={hp(2)} />
            </TouchableOpacity>
          </View>
        ) : null}
        <Text
          style={{
            textAlign: 'center',
            textAlignVertical: 'center',
            flex: 6,
            fontSize: hp(3),
            color: colors.primarydark,
            fontFamily: 'JosefinSans-Bold',
          }}>
          {props.title}
        </Text>
        {/* {props.aboutButton ? (
          <View style={{flex: 0.5, alignItems: 'center'}}>
            <TouchableOpacity
              onPress={() => props.showSideBar()}
              style={{flex: 1, justifyContent: 'center'}}
              transparent>
              <Icon
                name="info"
                size={hp('4%')}
                style={{color: 'white', marginTop: 2}}
              />
            </TouchableOpacity>
          </View>
        ) : null} */}
        {props.rightButton ? (
          <View style={{position: 'absolute', zIndex: 1000, right: 0}}>
            <TouchableOpacity onPress={props.onRightButtonPress}>
              <FontAweSome
                name={
                  props.rightButton.length > 0 ? props.rightButton : 'trash'
                }
                color={colors.primarydark}
                size={props.righButtonSize ? props.righButtonSize : hp(3)}
              />
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    </Header>
  );
};

export default DeafaultHeader;

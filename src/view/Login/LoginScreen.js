/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  ImageBackground,
  Keyboard,
  Alert,
} from 'react-native';
import {Container, Text} from 'native-base';

import AsyncStorage from '@react-native-async-storage/async-storage';

import ReTextInput from '../../component/ReTextInput';
import ReButton from '../../component/ReButton';
import ReLoadingModal from '../../component/ReLoadingModal';
import ReAlertModal from '../../component/ReAlertModal';
import colors from '../../res/colors/index';

import {useNavigation} from '@react-navigation/native';
import firestore from '@react-native-firebase/firestore';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const initialLogin = {
  username: '',
  password: '',
};

const LoginScreen = () => {
  const [loginForm, setLoginForm] = useState(initialLogin);
  const [state, setState] = useState({
    loading: false,
    successModal: false,

    titleDisplay: true,
  });

  const [loading, setLoading] = useState(false);
  const navigation = useNavigation();

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const LoginUser = async () => {
    await setLoading(true);
    let User;

    await firestore()
      .collection('user')
      .where('username', '==', loginForm.username)
      .where('password', '==', loginForm.password)
      .get()
      .then(async (snapshot) => {
        await snapshot.forEach((doc) => {
          if (doc) {
            User = {
              id: doc.id,
              username: doc.data().username,
              password: doc.data().password,
              nama: doc.data().nama,
              foto: doc.data().foto,
              role: doc.data().role,
            };
          }
        });
        console.log('user', User);
        if (User) {
          await AsyncStorage.setItem('userData', JSON.stringify(User));
          await navigation.navigate('HomeAdmin');
          setLoginForm(initialLogin);
        } else {
          Alert.alert('Gagal Login', 'Username / Password tidak sesuai');
        }
        setLoading(false);
      })
      .catch(() => {
        Alert.alert('Gagal Login', 'Check Koneksi Internet');
        setLoading(false);
      });
  };

  const checkFormLogin = () => {
    if (loginForm.username.length > 0 && loginForm.password.length > 0) {
      return false;
    }
    return true;
  };

  const onKeyboardDidShow = () => {
    updateState({titleDisplay: false});
  };

  const onKeyboardDidHide = () => {
    updateState({titleDisplay: true});
  };

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', onKeyboardDidShow);
    Keyboard.addListener('keyboardDidHide', onKeyboardDidHide);

    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', onKeyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', onKeyboardDidHide);
    };
  }, []);

  return (
    <Container
      style={{
        flex: 1,
        backgroundColor: colors.white,
      }}>
      <ImageBackground
        style={{flex: 1, backgroundColor: 'white'}}
        source={require('../../res/image/bg.jpeg')}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            // justifyContent: 'center',
            paddingHorizontal: wp(2),
            marginTop: hp(10),
          }}>
          {state.titleDisplay ? (
            <View style={{alignItems: 'center', marginBottom: wp(20)}}>
              <View style={{marginBottom: wp(4), alignItems: 'center'}}>
                <Text
                  style={{
                    fontSize: wp(9),
                    fontWeight: 'bold',
                    color: colors.gray06,
                    fontFamily: 'Poppins-Bold',
                  }}>
                  E-TKJI
                </Text>
                <Text
                  style={{
                    fontSize: wp(5),
                    color: colors.gray06,
                    fontFamily: 'Poppins-Regular',
                  }}>
                  Tes Kebugaran Jasmani Indonesia
                </Text>
              </View>
            </View>
          ) : null}

          <View style={{marginBottom: wp(2), marginTop: wp(5), width: wp(85)}}>
            <ReTextInput
              label="Username"
              value={loginForm.username}
              onChangeText={(val) => {
                setLoginForm({...loginForm, username: val});
              }}
              loginScreen
            />
          </View>
          <View style={{marginVertical: wp(2), width: wp(85)}}>
            <ReTextInput
              label="Password"
              value={loginForm.password}
              onChangeText={(val) => {
                setLoginForm({...loginForm, password: val});
              }}
              passwordInput
              loginScreen
            />
          </View>
          <View style={{marginVertical: wp(4), width: wp(85)}}>
            <ReButton
              label="LOGIN"
              disabled={checkFormLogin()}
              onPress={() => {
                LoginUser();
              }}
            />
          </View>
          <View
            style={{
              marginTop: wp(2),
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text style={{color: colors.white}}>{'-TKJI- V.1.2'}</Text>
            {/* <TouchableOpacity
              onPress={() => navigation.navigate('RegisterScreen')}>
              <Text style={{color: colors.white, fontWeight: 'bold'}}>
                Daftar
              </Text>
            </TouchableOpacity> */}
          </View>
        </View>
        <View
          style={{
            position: 'absolute',
            bottom: 10,
            alignItems: 'center',
            width: '100%',
          }}>
          {/* <Text style={{fontWeight: 'bold'}}>v.1.2.1</Text> */}
        </View>
      </ImageBackground>

      <ReLoadingModal visible={loading} title="Login.." />
      <ReAlertModal
        visible={state.successModal}
        title="Gagal Login"
        subTitle="Cek Email dan Password"
        onConfirm={() => {
          setState({...state, successModal: false});
        }}
      />
      {/* <Footer
        style={{
          backgroundColor: colors.white,
          borderTopColor: colors.gray05,
          borderWidth: 0.5,
        }}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <Text style={{color: colors.gray02}}>
            {'Dont have an account ?  '}
          </Text>
          <TouchableOpacity
            onPress={() => navigation.navigate('RegisterScreen')}>
            <Text style={{color: colors.gray02, fontWeight: 'bold'}}>
              Sign Up
            </Text>
          </TouchableOpacity>
        </View>
      </Footer> */}
    </Container>
  );
};

export default LoginScreen;

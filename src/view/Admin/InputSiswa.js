/* eslint-disable no-alert */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {Container, Content} from 'native-base';

import colors from '../../res/colors/index';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import uuid from 'react-native-uuid';

import DeafaultHeader from '../../component/DeafaultHeader';
import ReTextInput from '../../component/ReTextInput';
import ReTextArea from '../../component/ReTextArea';

import ReModalImagePicker from '../../component/ReModalImagePicker';
import ReButton from '../../component/ReButton';
import ReLoadingModal from '../../component/ReLoadingModal';

import {useNavigation, useRoute} from '@react-navigation/native';
import ReLoading from '../../component/ReLoading';
import RePicker from '../../component/RePicker';

const initialState = {
  loading: true,
  loadingModal: false,
  id: '',

  nama: '',
  npm: '',
  kelas: '',
  jurusan: '',

  kelasList: null,
  jurusanList: null,

  siswa: {},
  visiblePicker: false,
};

const InputSiswa = () => {
  const route = useRoute();
  const navigation = useNavigation();

  const [state, setState] = useState(initialState);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const togglePicker = () => {
    updateState({visiblePicker: !state.visiblePicker});
  };

  const preparedFoto = async (foto) => {
    if (foto.length === 0) {
      console.log('jalaan');

      return '';
    } else {
      const uri = foto;
      const ext = uri.split('.').pop();
      const filename = `${uuid.v1()}.${ext}`;
      const imageRef = storage().ref(`foto/${filename}`);
      await imageRef.putFile(uri);
      const url = await imageRef.getDownloadURL();

      return url;
    }
  };

  const preparedData = async () => {
    return {
      nama: state.nama,
      npm: state.npm,
      kelas: state.kelas,
      jurusan: state.jurusan,
      createAt: firestore.FieldValue.serverTimestamp(),
    };
  };

  const onSaveUser = async () => {
    await updateState({loadingModal: true});
    const newData = await preparedData();
    console.log('new data', newData);
    const userCollection = firestore().collection('siswa');

    await userCollection
      .add({
        nama: newData.nama,
        npm: newData.npm,
        kelas: newData.kelas,
        jurusan: newData.jurusan,
        createAt: newData.createAt,
      })
      .then(() => {
        alert('Berhasil menyimpan data');
        setState(initialState);
        navigation.goBack();
      })
      .catch(() => {
        alert('Gagal menyimpan data');
      });
    await updateState({loadingModal: false});
  };

  const onUpdateUser = async () => {
    await updateState({loadingModal: true});

    const newData = await preparedData();
    console.log('new data', newData);
    const userCollection = firestore().collection('siswa');

    userCollection
      .doc(state.id)
      .update({
        nama: newData.nama,
        npm: newData.npm,
        kelas: newData.kelas,
        jurusan: newData.jurusan,
        createAt: newData.createAt,
      })
      .then(() => {
        console.log('jalan update');
        alert('Berhasil menyimpan data');
        setState(initialState);
        navigation.goBack();
      })
      .catch(() => {
        alert('Gagal menyimpan data');
      });
    await updateState({loadingModal: false});
  };

  const loadJurusan = async () => {
    const jurusankCollection = firestore().collection('jurusan');

    const jurusan = await jurusankCollection.get().then((querySnapshot) => {
      let list = [];
      querySnapshot.forEach((doc) => {
        list.push({
          value: doc.data().nama,
          label: doc.data().nama,
        });
      });
      return list;
    });
    await updateState({jurusanList: jurusan});
  };

  const loadKelas = async () => {
    const kelasCollection = firestore().collection('kelas');

    const kelas = await kelasCollection.get().then((querySnapshot) => {
      let list = [];
      querySnapshot.forEach((doc) => {
        list.push({
          value: doc.data().nama,
          label: doc.data().nama,
        });
      });
      return list;
    });
    await updateState({kelasList: kelas});
  };

  const checkParamsData = async () => {
    await updateState({loading: true});

    await loadJurusan();
    await loadKelas();

    const user = (await route.params) ? route.params.siswa : null;
    if (user !== null) {
      await updateState({
        id: user.id,
        nama: user.nama,
        npm: user.npm,
        kelas: user.kelas,
        jurusan: user.jurusan,
        createAt: user.createAt,
      });
    }
    await updateState({loading: false});
  };

  useEffect(() => {
    checkParamsData();
  }, []);

  const checkDisableSave = () => {
    if (
      state.nama.length > 0 &&
      state.npm.length > 0 &&
      state.kelas.length > 0 &&
      state.jurusan.length > 0
    ) {
      return false;
    }
    return true;
  };

  return (
    <Container>
      <ImageBackground
        style={{flex: 1, backgroundColor: 'white'}}
        source={require('../../res/image/background.jpg')}>
        <DeafaultHeader
          title="Input Data Siswa"
          backButton
          // rightButton={state.user.role === 'admin'}
          // onMenuBarPress={() => {
          //   toggleVisibleSideBar();
          // }}
        />
        {state.loading ? (
          <ReLoading />
        ) : (
          <Content style={{paddingHorizontal: wp(7), paddingVertical: hp(1.5)}}>
            <View style={{}}>
              <View style={{marginBottom: hp(1), marginTop: hp(2)}}>
                <ReTextInput
                  label="NPM"
                  value={state.npm}
                  onChangeText={(val) => {
                    updateState({npm: val});
                  }}
                  placeholder="Input NPM"
                  labelColor={colors.primarydark}
                  numeric
                />
              </View>
              <View style={{marginBottom: hp(1), marginTop: hp(1)}}>
                <ReTextInput
                  label="Nama"
                  value={state.nama}
                  onChangeText={(val) => {
                    updateState({nama: val});
                  }}
                  placeholder="Input nama"
                  labelColor={colors.primarydark}
                />
              </View>
              <View style={{marginVertical: hp(1)}}>
                <RePicker
                  label="Pilih Kelas"
                  items={state.kelasList}
                  selectedValue={state.kelas}
                  noAnimation
                  onValueChange={(value) => {
                    updateState({
                      kelas: value,
                    });
                  }}
                />
              </View>
              <View style={{marginVertical: hp(1)}}>
                <RePicker
                  label="Pilih Jurusan"
                  items={state.jurusanList}
                  selectedValue={state.jurusan}
                  noAnimation
                  onValueChange={(value) => {
                    updateState({
                      jurusan: value,
                    });
                  }}
                />
              </View>
              <View style={{alignItems: 'center', marginTop: hp(5)}}>
                <View style={{width: wp(80)}}>
                  <ReButton
                    label="Simpan"
                    disabled={checkDisableSave()}
                    onPress={() => {
                      if (state.id) {
                        onUpdateUser(state.id);
                      } else {
                        onSaveUser();
                      }
                    }}
                  />
                </View>
              </View>
            </View>
            <View style={{height: hp(10)}} />
            <ReLoadingModal
              visible={state.loadingModal}
              title="Menyimpan Data Siswa.."
            />
          </Content>
        )}

        <ReModalImagePicker
          visible={state.visiblePicker}
          onCancel={() => togglePicker()}
          onPickImage={(image) => {
            console.log(image);
            togglePicker();
            updateState({
              fotoObject: image,
              foto: image.uri,
            });
          }}
        />
      </ImageBackground>
    </Container>
  );
};

export default InputSiswa;

/* eslint-disable no-alert */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {Container, Content} from 'native-base';

import colors from '../../res/colors/index';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import uuid from 'react-native-uuid';

import DeafaultHeader from '../../component/DeafaultHeader';
import ReTextInput from '../../component/ReTextInput';
import ReTextArea from '../../component/ReTextArea';

import ReModalImagePicker from '../../component/ReModalImagePicker';
import ReButton from '../../component/ReButton';
import ReLoadingModal from '../../component/ReLoadingModal';

import {useNavigation, useRoute} from '@react-navigation/native';
import ReLoading from '../../component/ReLoading';

const initialState = {
  loading: true,
  id: '',
  nama: '',
  foto: '',
  deskripsi: '',

  fotoObject: {},

  user: {},
  visiblePicker: false,
};

const InputTes = () => {
  const route = useRoute();
  const navigation = useNavigation();

  const [state, setState] = useState(initialState);
  const [loading, setLoading] = useState(false);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const togglePicker = () => {
    updateState({visiblePicker: !state.visiblePicker});
  };

  const preparedFoto = async (foto) => {
    if (foto.length === 0) {
      console.log('jalaan');

      return '';
    } else {
      console.log('jalaan 2', foto);

      const uri = foto;
      const ext = uri.split('.').pop();
      const filename = `${uuid.v1()}.${ext}`;
      const imageRef = storage().ref(`foto/${filename}`);
      await imageRef.putFile(uri);
      const url = await imageRef.getDownloadURL();

      return url;
    }
  };

  const preparedData = async () => {
    const fotoUri =
      (await Object.keys(state.fotoObject).length) > 0
        ? await preparedFoto(state.foto)
        : state.foto;

    return {
      nama: state.nama,
      deskripsi: state.deskripsi,
      foto: fotoUri,
      createAt: firestore.FieldValue.serverTimestamp(),
    };
  };

  const tesCollection = firestore().collection('tes');

  const onSaveTes = async () => {
    await setLoading(true);
    const newData = await preparedData();
    console.log('new data', newData);

    await tesCollection
      .add({
        ...newData,
      })
      .then(() => {
        alert('Berhasil menyimpan data');
        setState(initialState);
        navigation.goBack();
      })
      .catch(() => {
        alert('Gagal menyimpan data');
      });
    await setLoading(false);
  };

  const onUpdateTes = async (docId) => {
    await setLoading(true);
    const newData = await preparedData();
    console.log('new data', newData);

    await tesCollection
      .doc(docId)
      .update({...newData})
      .then(() => {
        alert('Berhasil menyimpan data');
        setState(initialState);
        navigation.goBack();
      })
      .catch(() => {
        alert('Gagal menyimpan data');
      });
    await setLoading(false);
  };

  const checkParamsData = async () => {
    await updateState({loading: true});

    const tesDetail = (await route.params) ? route.params.tes : null;
    if (tesDetail !== null) {
      await updateState({
        id: tesDetail.id,
        foto: tesDetail.foto,
        nama: tesDetail.nama,
        deskripsi: tesDetail.deskripsi,
        createAt: tesDetail.createAt,
      });
    }
    await updateState({loading: false});
  };

  useEffect(() => {
    checkParamsData();
  }, []);

  const checkDisableSave = () => {
    if (
      state.foto.length > 0 &&
      state.nama.length > 0 &&
      state.deskripsi.length > 0
    ) {
      return false;
    }
    return true;
  };

  return (
    <Container>
      <ImageBackground
        style={{flex: 1, backgroundColor: 'white'}}
        source={require('../../res/image/background.jpg')}>
        <DeafaultHeader
          title="Input Data Tes"
          backButton
          rightButton={state.user.role === 'admin'}
          // onMenuBarPress={() => {
          //   toggleVisibleSideBar();
          // }}
        />
        {state.loading ? (
          <ReLoading />
        ) : (
          <Content style={{paddingHorizontal: wp(5), paddingVertical: hp(1.5)}}>
            <View style={{marginLeft: wp(2.2), marginVertical: wp(2.5)}}>
              <View>
                <ReTextInput
                  label="Nama Tes"
                  value={state.nama}
                  onChangeText={(val) => {
                    updateState({nama: val});
                  }}
                  placeholder="Input nama tes"
                  labelColor={colors.primarydark}
                />
              </View>
              <View style={{marginTop: hp(3)}}>
                <ReTextArea
                  label="Deskripsi"
                  placeholder="Input deskripsi"
                  // labelColor={colors.primarydark}
                  value={state.deskripsi}
                  multiLine
                  onChangeText={(val) => {
                    updateState({deskripsi: val});
                  }}
                  noAnimation
                />
              </View>
              <View style={{marginVertical: wp(2), width: wp(90)}}>
                <Text
                  style={{
                    fontSize: wp(4),
                    // fontWeight: 'bold',
                    marginBottom: hp(1),
                  }}>
                  Tambah Gambar
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    togglePicker();
                  }}
                  style={{
                    height: hp(24),
                    backgroundColor: colors.gray01,
                    borderRadius: wp(1),
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  {state.foto.length > 0 ? (
                    <Image
                      source={{
                        uri: state.foto,
                      }}
                      style={{
                        height: hp(22),
                        width: hp(100),
                        // borderRadius: hp(20),
                        resizeMode: 'contain',
                      }}
                    />
                  ) : (
                    <FontAwesome
                      name="camera"
                      color={colors.primarydark}
                      size={wp(8)}
                    />
                  )}
                </TouchableOpacity>
              </View>
              <View style={{marginVertical: wp(4), width: wp(85)}}>
                <ReButton
                  label="Simpan"
                  disabled={checkDisableSave()}
                  onPress={() => {
                    // onSaveTes();
                    if (state.id) {
                      onUpdateTes(state.id);
                    } else {
                      onSaveTes();
                    }
                  }}
                />
              </View>
            </View>
            <View style={{height: hp(10)}} />
          </Content>
        )}

        <ReLoadingModal visible={loading} title="Menyimpan Tes.." />

        <ReModalImagePicker
          visible={state.visiblePicker}
          onCancel={() => togglePicker()}
          onPickImage={(image) => {
            console.log(image);
            togglePicker();
            updateState({
              fotoObject: image,
              foto: image.uri,
            });
          }}
        />
      </ImageBackground>
    </Container>
  );
};

export default InputTes;

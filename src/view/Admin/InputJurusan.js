/* eslint-disable no-alert */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  ImageBackground,
} from 'react-native';
import {Container, Content} from 'native-base';

import colors from '../../res/colors/index';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import uuid from 'react-native-uuid';

import DeafaultHeader from '../../component/DeafaultHeader';
import ReTextInput from '../../component/ReTextInput';

import ReModalImagePicker from '../../component/ReModalImagePicker';
import ReButton from '../../component/ReButton';
import ReLoadingModal from '../../component/ReLoadingModal';

import {useNavigation, useRoute} from '@react-navigation/native';
import ReLoading from '../../component/ReLoading';

const initialState = {
  loading: true,
  id: '',

  nama: '',
  username: '',
  password: '',
  role: '',
  foto: '',

  fotoObject: {},

  user: {},
  visiblePicker: false,
};


const InputJurusan = () => {
  const route = useRoute();
  const navigation = useNavigation();

  const [state, setState] = useState(initialState);
  const [loading, setLoading] = useState(false);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const togglePicker = () => {
    updateState({visiblePicker: !state.visiblePicker});
  };

  const preparedFoto = async (foto) => {
    if (foto.length === 0) {
      console.log('jalaan');

      return '';
    } else {
      console.log('jalaan 2', foto);

      const uri = foto;
      const ext = uri.split('.').pop();
      const filename = `${uuid.v1()}.${ext}`;
      const imageRef = storage().ref(`foto/${filename}`);
      await imageRef.putFile(uri);
      const url = await imageRef.getDownloadURL();

      return url;
    }
  };

  const preparedData = async () => {
    const fotoUri =
      (await Object.keys(state.fotoObject).length) > 0
        ? await preparedFoto(state.foto)
        : state.foto;

    return {
      nama: state.nama,
      username: state.username,
      password: state.password,
      role: state.role,
      foto: fotoUri,
      createAt: firestore.FieldValue.serverTimestamp(),
    };
  };

  const jurusanCollection = firestore().collection('jurusan');

  const onSaveUser = async () => {
    await setLoading(true);
    const newData = await preparedData();
    console.log('new data', newData);

    await jurusanCollection
      .add({
        ...newData,
      })
      .then(() => {
        alert('Berhasil menyimpan data');
        setState(initialState);
        navigation.goBack();
      })
      .catch(() => {
        alert('Gagal menyimpan data');
      });
    await setLoading(false);
  };

  const onUpdateUser = async (docId) => {
    await setLoading(true);
    const newData = await preparedData();
    console.log('new data', newData);

    await jurusanCollection
      .doc(docId)
      .update({...newData})
      .then(() => {
        alert('Berhasil menyimpan data');
        setState(initialState);
        navigation.goBack();
      })
      .catch(() => {
        alert('Gagal menyimpan data');
      });
    await setLoading(false);
  };

  const checkParamsData = async () => {
    await updateState({loading: true});

    const jurusan = (await route.params) ? route.params.jurusan : null;

    console.log('jurus', jurusan);
    if (jurusan !== null) {
      await updateState({
        id: jurusan.id,
        nama: jurusan.nama,
        createAt: jurusan.createAt,
      });
    }
    await updateState({loading: false});
  };

  useEffect(() => {
    checkParamsData();
  }, []);

  const checkDisableSave = () => {
    if (state.nama.length > 0) {
      return false;
    }
    return true;
  };

  return (
    <Container>
      <ImageBackground
        style={{flex: 1, backgroundColor: 'white'}}
        source={require('../../res/image/background.jpg')}>
        <DeafaultHeader
          title="Input Jurusan"
          backButton
          rightButton={state.user.role === 'admin'}
          // onMenuBarPress={() => {
          //   toggleVisibleSideBar();
          // }}
        />
        {state.loading ? (
          <ReLoading />
        ) : (
          <Content style={{paddingHorizontal: wp(7), paddingVertical: hp(1.5)}}>
            <View style={{}}>
              <View style={{marginBottom: hp(1), marginTop: hp(2)}}>
                <ReTextInput
                  label="Nama Jurusan"
                  value={state.nama}
                  onChangeText={(val) => {
                    updateState({nama: val});
                  }}
                  placeholder="Input nama jurusan"
                  labelColor={colors.primarydark}
                />
              </View>
              <View style={{alignItems: 'center', marginTop: hp(5)}}>
                <View style={{width: wp(80)}}>
                  <ReButton
                    label="Simpan"
                    disabled={checkDisableSave()}
                    onPress={() => {
                      if (state.id) {
                        onUpdateUser(state.id);
                      } else {
                        onSaveUser();
                      }
                    }}
                  />
                </View>
              </View>
            </View>
            <View style={{height: hp(10)}} />
          </Content>
        )}

        <ReLoadingModal visible={loading} title="Menyimpan Tes.." />

        <ReModalImagePicker
          visible={state.visiblePicker}
          onCancel={() => togglePicker()}
          onPickImage={(image) => {
            console.log(image);
            togglePicker();
            updateState({
              foto: image.uri,
            });
          }}
        />
      </ImageBackground>
    </Container>
  );
};

export default InputJurusan;

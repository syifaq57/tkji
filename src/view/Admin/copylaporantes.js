/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  Alert,
  PermissionsAndroid,
  ActivityIndicator,
} from 'react-native';
import { Container, Content } from 'native-base';
import firestore from '@react-native-firebase/firestore';
import DeafaultHeader from '../../component/DeafaultHeader';
import ReLoading from '../../component/ReLoading';
import ReLoadingModal from '../../component/ReLoadingModal';
import SideBar from '../../component/SideBar';
import ReDatePicker from '../../component/ReDatePicker';
import ReButton from '../../component/ReButton';

import colors from '../../res/colors';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import ReModal from 'react-native-modal';

import { useNavigation, useIsFocused } from '@react-navigation/native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import RNFS, { writeFile } from 'react-native-fs';

import FileViewer from 'react-native-file-viewer';

import XLSX from 'xlsx';
import RePicker from '../../component/RePicker';

const initialState = {
  loading: false,
  loadingModal: false,
  visibleSideBar: false,
  toggleFilter: false,

  date: '',
  tesSelected: {},
};

const roleList = [
  { value: 'User', label: 'User' },
  { value: 'Admin', label: 'Admin' },
];

const initialTable = {
  tableHead: [
    { id: 'nama', label: 'Nama', width: 10 },
    { id: 'kelas', label: 'Kelas', width: 10 },
    { id: 'jurusan', label: 'Jurusan', width: 10 },
    { id: 'nilai', label: 'Nilai', width: 10 },
    { id: 'namaTes', label: 'Nama Tes', width: 10 },
    { id: 'tanggal', label: 'Periode', width: 10 },
  ],
  tableData: [],
};

const initialFilter = {
  typeTes: '',
  jurusan: '',
  kelas: '',
  date: '',

  listJurusan: [],
  listKelas: [],
  listTes: [],
};

const LaporanTes = () => {
  const navigation = useNavigation();

  const isFocused = useIsFocused();

  const [state, setState] = useState(initialState);

  const [table, setTable] = useState(initialTable);

  const [filter, setFilter] = useState(initialFilter);

  const updateState = (newData) => {
    setState((prev) => ({ ...prev, ...newData }));
  };

  const updateFilter = (newData) => {
    setFilter((prev) => ({ ...prev, ...newData }));
  };

  const resetReport = () => {
    updateState({
      tesSelected: {},
    });
    updateFilter({
      typeTes: '',
      jurusan: '',
      kelas: '',
      date: '',
    });
  };

  const toggleVisibleSideBar = () => {
    updateState({ visibleSideBar: !state.visibleSideBar });
  };

  const normalizeDate = (date) => {
    const year = new Date(date).getFullYear();

    const mont = new Date(date).getMonth();

    const day = new Date(date).getDate();

    const currentDate = day + '/' + mont + '/' + year;

    return currentDate;
  };

  const dateToTime = (date) => {
    var currentdate = new Date(date);
    let start = new Date(currentdate.toLocaleDateString());
    return start.getTime();
  };

  const loadTes = async () => {
    let dataTes = await firestore().collection('tesList');

    await updateState({ loading: true });

    const time = dateToTime(filter.date);
    // if (filter.date.length > 0) {
    //   dataTes = dataTes.where('tanggal', '==', time);
    // } else {
    //   dataTes = dataTes.orderBy('createAt', 'desc');
    // }

    await dataTes
      .orderBy('createAt', 'desc')
      .get()
      .then((snapshot) => {
        let list = [];
        snapshot.forEach((doc) => {
          list.push({
            id: doc.id,
            nama: doc.data().nama,
            kelas: doc.data().kelas,
            jurusan: doc.data().jurusan,
            nilai: doc.data().nilai,
            tanggal: normalizeDate(doc.data().tanggal),
            periode: doc.data().tanggal,
            namaTes: doc.data().namaTes,
            idTes: doc.data().idTes,
            createAt: doc.data().createAt,
          });
        });
        setTable({ ...table, tableData: list });
      });

    let kelas = await firestore().collection('kelas');
    let jurusan = await firestore().collection('jurusan');
    let tes = await firestore().collection('tes');

    await kelas
      .orderBy('createAt', 'desc')
      .get()
      .then((snapshot) => {
        let list = [];
        snapshot.forEach((doc) => {
          list.push({
            value: doc.data().nama,
            label: doc.data().nama,
          });
        });
        updateFilter({ listKelas: list });
      });

    await tes
      .orderBy('createAt', 'desc')
      .get()
      .then((snapshot) => {
        let list = [];
        snapshot.forEach((doc) => {
          list.push({
            value: doc.id,
            label: doc.data().nama,
          });
        });
        updateFilter({ listTes: list });
      });

    await jurusan
      .orderBy('createAt', 'desc')
      .get()
      .then((snapshot) => {
        let list = [];
        snapshot.forEach((doc) => {
          list.push({
            value: doc.data().nama,
            label: doc.data().nama,
          });
        });
        updateFilter({ listJurusan: list });
      });
    updateState({ loading: false });
  };

  const exportExcel = async (data, date) => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Akses Dokumen',
          message: 'Aplikasi membutuhkan akses untuk menyimpan file',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // console.log('You can use the camera');
        const ws = await XLSX.utils.json_to_sheet(data);
        const wb = await XLSX.utils.book_new();
        await XLSX.utils.book_append_sheet(wb, ws, 'SheetJS');
        ws['!cols'] = [
          { width: 5 },
          { width: 20 },
          { width: 25 },
          { width: 20 },
          { width: 25 },
          { width: 20 },
        ];

        const wbout = await XLSX.write(wb, { type: 'binary', bookType: 'xlsx' });
        const file = RNFS.DownloadDirectoryPath + '/' + `report_${date}.xlsx`;
        await writeFile(file, wbout, 'ascii')
          .then(async () => {
            Alert.alert('Export Excel', `File telah di export ke ${file}?`, [
              {
                text: 'Tutup',
                onPress: () => {
                  // return true;
                },
                style: 'cancel',
              },
              {
                text: 'Buka File',
                onPress: () => {
                  FileViewer.open(file);
                },
              },
            ]);
          })
          .catch((err) => {
            Alert.alert('exportFile Error', 'Error ' + err.message);
          });
      } else {
        console.log('permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
    /* convert AOA back to worksheet */
  };

  const onPreparedExport = async () => {
    const newData = await table.tableData.map((data, index) => {
      return {
        No: index + 1,
        Nama: data.nama,
        Kelas: data.kelas,
        Jurusan: data.jurusan,
        Nilai: data.nilai,
        NamaTes: data.namaTes,
        Periode: data.tanggal,
      };
    });

    const tanggal =
      filter.date.length > 0 ? dateToTime(filter.date) : 'ALL_DATA';

    await exportExcel(newData, tanggal);
  };

  const toggleFilter = () => {
    updateState({ toggleFilter: !state.toggleFilter });
  };

  const getTableData = () => {
    let result = table.tableData;
    // const time = dateToTime(filter.date);

    if (filter.typeTes.length > 0) {
      result = result.filter((v) => {
        return v.idTes.includes(filter.typeTes);
      });
    }

    if (filter.jurusan.length > 0) {
      result = result.filter((v) => {
        return v.jurusan.includes(filter.jurusan);
      });
    }

    if (filter.kelas.length > 0) {
      result = result.filter((v) => {
        return v.kelas.includes(filter.kelas);
      });
    }

    if (filter.date.length > 0) {
      result = result.filter((v) => {
        return v.periode.toString().includes(dateToTime(filter.date));
      });
    }
    return result;
  };

  useEffect(() => {
    loadTes();
  }, [isFocused]);

  const onFilter = (item) => {
    updateState({
      toggleFilter: false,
    });
    updateFilter({
      typeTes: item.typeTes,
      jurusan: item.jurusan,
      kelas: item.kelas,
    });
  };

  const FilterTesModal = (props) => {
    const initialLocalState = {
      typeTes: '',
      jurusan: '',
      kelas: '',
      date: '',
    };
    const [localState, setLocalState] = useState(initialLocalState);

    const updateLocalState = (newData) => {
      setLocalState((prev) => ({ ...prev, ...newData }));
    };

    return (
      <ReModal
        isVisible={state.toggleFilter}
        onBackdropPress={() => {
          toggleFilter();
        }}
        onBackButtonPress={() => {
          toggleFilter();
        }}
        style={{
          alignItems: 'center',
          justifyContent: 'flex-start',
          paddingVertical: hp(2),
        }}
        backdropOpacity={0.7}
        animationIn={'fadeIn'}
        animationOut={'fadeOut'}
        useNativeDriver={true}>
        <View
          style={{
            backgroundColor: 'white',
            borderRadius: wp(1),
            padding: hp('1%'),
            width: wp(90),
            // height: hp(80),
          }}>
          <View
            style={{
              paddingTop: hp(1),
              paddingBottom: hp(1.5),
              borderBottomWidth: 0.5,
              borderColor: 'gray',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: wp(4),
                color: colors.primarydark,
                fontWeight: 'bold',
              }}>
              Filter Tes
            </Text>
          </View>
          <ScrollView
            style={{
              paddingVertical: hp(2),
              paddingBottom: hp(4),
              paddingHorizontal: wp(4),
            }}>
            <View style={{ marginVertical: hp(2) }}>
              <RePicker
                label="Pilih Jenis Tes"
                items={filter.listTes}
                selectedValue={localState.typeTes}
                noAnimation
                onValueChange={(value) => {
                  updateLocalState({
                    typeTes: value,
                  });
                }}
              />
            </View>
            <View style={{ marginVertical: hp(2) }}>
              <RePicker
                label="Pilih Kelas"
                items={filter.listKelas}
                selectedValue={localState.kelas}
                noAnimation
                onValueChange={(value) => {
                  updateLocalState({
                    kelas: value,
                  });
                }}
              />
            </View>
            <View style={{ marginVertical: hp(2) }}>
              <RePicker
                label="Pilih Jurusan"
                items={filter.listJurusan}
                selectedValue={localState.jurusan}
                noAnimation
                onValueChange={(value) => {
                  updateLocalState({
                    jurusan: value,
                  });
                }}
              />
            </View>
            <View style={{ alignItems: 'center', marginTop: hp(5) }}>
              <View style={{ width: wp(80) }}>
                <ReButton
                  label="Filter"
                  // disabled={checkDisableSave()}
                  onPress={() => {
                    onFilter(localState);
                  }}
                />
              </View>
            </View>
          </ScrollView>
        </View>
      </ReModal>
    );
  };

  return (
    <Container>
      <ImageBackground
        style={{ flex: 1, backgroundColor: 'white' }}
        source={require('../../res/image/background.jpg')}>
        <DeafaultHeader
          title="Laporan & Hasil Tes"
          menuBar
          onMenuBarPress={() => {
            toggleVisibleSideBar();
          }}
          rightButton={
            Object.keys(state.tesSelected).length > 0 ? 'pencil' : false
          }
          onRightButtonPress={() => {
            // deleteTes();
            navigation.navigate('InputNilaiTes', {
              edited: true,
              detailTes: state.tesSelected,
              namaTes: {
                nama: state.tesSelected.namaTes,
                id: state.tesSelected.namaTes,
              },
            });
          }}
        />
        <View
          style={{
            paddingHorizontal: wp(5.2),
            marginVertical: hp(1),
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <View style={{ flex: 3, marginRight: wp(1) }}>
            <ReDatePicker
              placeholder="Periode Tes"
              label="Periode Tes"
              selectedDate={filter.date}
              onDateChange={(date) => {
                updateFilter({ date: date });
              }}
            />
          </View>
          <View style={{ flex: 1, marginTop: hp(1), alignItems: 'flex-end' }}>
            <TouchableOpacity
              onPress={() => {
                resetReport();
              }}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: colors.primarydark,
                width: wp(9),
                height: wp(9),
                borderRadius: wp(1),
              }}>
              <FontAweSome name="undo" color={colors.white} size={hp(3)} />
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1, marginTop: hp(1), alignItems: 'center' }}>
            <TouchableOpacity
              onPress={() => toggleFilter()}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: colors.primarydark,
                width: wp(9),
                height: wp(9),
                borderRadius: wp(1),
              }}>
              <FontAweSome name="filter" color={colors.white} size={hp(3)} />
            </TouchableOpacity>
          </View>
          <View style={{ flex: 2, marginTop: hp(1), alignItems: 'center' }}>
            <TouchableOpacity
              onPress={() => {
                onPreparedExport();
              }}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: colors.primarydark,
                width: '100%',
                height: wp(9),
                borderRadius: wp(1),
              }}>
              <Text
                style={{
                  fontSize: wp(3.2),
                  fontFamily: 'JosefinSans-Medium',
                  color: 'white',
                }}>
                EXPORT
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        {state.loading ? (
          <ReLoading />
        ) : (
            <Content style={{ paddingHorizontal: wp(5), paddingVertical: hp(1.5) }}>
              <View>
                <ScrollView horizontal={true}>
                  <View
                    style={{
                      borderWidth: 1,
                      borderColor: colors.gray11,
                      backgroundColor: colors.primarydark,
                      flexDirection: 'row',
                    }}>
                    {table.tableHead.map((data, indexHeader) => (
                      <View key={indexHeader}>
                        <View
                          style={{
                            paddingVertical: hp(1),
                            paddingHorizontal: wp(2),
                            borderRightWidth:
                              indexHeader === table.tableHead.length - 1 ? 0 : 1,
                            borderBottomWidth: 1,
                            borderColor: colors.gray11,
                          }}>
                          <Text
                            style={{
                              fontSize: wp(3.2),
                              fontFamily: 'JosefinSans-Medium',
                              color: 'white',
                            }}>
                            {data.label}
                          </Text>
                        </View>
                        {getTableData().map((v, index) => (
                          <TouchableOpacity
                            key={index}
                            onPress={() => {
                              updateState({ tesSelected: v });
                            }}>
                            <View
                              style={{
                                paddingHorizontal: wp(2),
                                backgroundColor:
                                  v === state.tesSelected
                                    ? colors.orangeDefault
                                    : !index % 2
                                      ? colors.lightGray
                                      : colors.gray01,
                                paddingVertical: hp(1),
                                borderRightWidth:
                                  indexHeader === table.tableHead.length - 1
                                    ? 0
                                    : 1,
                                borderColor: colors.gray11,
                              }}>
                              <View style={{}}>
                                <Text
                                  style={{
                                    fontSize: wp(3.2),
                                    fontFamily: 'JosefinSans-Regular',
                                    color: colors.gray08,
                                  }}>
                                  {v[data.id]}
                                </Text>
                              </View>
                            </View>
                          </TouchableOpacity>
                        ))}
                      </View>
                    ))}
                  </View>
                </ScrollView>
              </View>
              <View style={{ height: hp(10) }} />
            </Content>
          )}
        <SideBar
          name={state.nama}
          isVisible={state.visibleSideBar}
          onBackButtonPress={() => toggleVisibleSideBar()}
        />
        <FilterTesModal />
        <ReLoadingModal visible={state.loading} title="Loading.." />
      </ImageBackground>
    </Container>
  );
};

export default LaporanTes;

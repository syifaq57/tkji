/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {Container, Content, Card} from 'native-base';
import firestore from '@react-native-firebase/firestore';
import DeafaultHeader from '../../component/DeafaultHeader';
import ReLoading from '../../component/ReLoading';
import ReLoadingModal from '../../component/ReLoadingModal';
import SideBar from '../../component/SideBar';

import colors from '../../res/colors';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';

import {useNavigation} from '@react-navigation/native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const initialState = {
  loading: false,
  visibleSideBar: false,
};

const DataTes = () => {
  const navigation = useNavigation();
  const [state, setState] = useState(initialState);

  const [tesList, setTesList] = useState([]);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const toggleVisibleSideBar = () => {
    updateState({visibleSideBar: !state.visibleSideBar});
  };

  const dataTes = firestore().collection('tes');

  const loadTes = async () => {
    await updateState({loading: true});
    await dataTes.orderBy('createAt', 'desc').onSnapshot((snapshot) => {
      let list = [];
      snapshot.forEach((doc) => {
        list.push({
          id: doc.id,
          foto: doc.data().foto,
          nama: doc.data().nama,
          deskripsi: doc.data().deskripsi,
          createAt: doc.data().createAt,
        });
      });
      setTesList(list);
    });
    updateState({loading: false});
  };

  const onOpenEditTes = (item) => {
    navigation.navigate('InputTes', {tes: item});
  };

  const deleteUser = (data) => {
    Alert.alert('Hapus Tes!', 'Apakah Kamu yakin ingin menghapus Tes?', [
      {
        text: 'Cancel',
        onPress: () => {
          // return true;
        },
        style: 'cancel',
      },
      {
        text: 'YES',
        onPress: () => {
          dataTes.doc(data.id).delete();
        },
      },
    ]);
  };

  useEffect(() => {
    loadTes();
  }, []);

  return (
    <Container>
      <ImageBackground
        style={{flex: 1, backgroundColor: 'white'}}
        source={require('../../res/image/background.jpg')}>
        <DeafaultHeader
          title="Informasi & Data Tes"
          menuBar
          onMenuBarPress={() => {
            toggleVisibleSideBar();
          }}
          rightButton="plus"
          onRightButtonPress={() => {
            navigation.navigate('InputTes');
          }}
        />
        {state.loading ? (
          <ReLoading />
        ) : (
          <Content style={{paddingHorizontal: wp(5), paddingVertical: hp(1.5)}}>
            <View>
              {tesList.map((data, index) => (
                <TouchableOpacity
                  key={index}
                  onPress={() => onOpenEditTes(data)}>
                  <Card
                    style={{
                      marginVertical: hp(2),
                      borderRadius: wp(2),
                      backgroundColor: colors.primary,
                    }}>
                    <LinearGradient
                      start={{x: 0.0, y: 0.25}}
                      end={{x: 0.5, y: 1.0}}
                      colors={[colors.primary, colors.bgErm]}
                      style={{
                        flexDirection: 'row',
                        borderRadius: wp(2),
                        height: hp(10),
                        alignItems: 'center',
                        paddingHorizontal: wp(3),
                      }}>
                      <View style={{flex: 1, marginRight: wp(2)}}>
                        <Image
                          source={
                            data.foto
                              ? {
                                  uri: data.foto,
                                }
                              : require('../../res/image/bg.jpeg')
                          }
                          style={{
                            height: hp(6),
                            width: hp(6),
                            borderRadius: hp(7),
                            resizeMode: 'cover',
                          }}
                        />
                      </View>
                      <View style={{flex: 6}}>
                        <Text
                          ellipsizeMode="tail"
                          numberOfLines={1}
                          style={{
                            color: colors.primarydark,
                            fontSize: wp(3.2),
                            fontFamily: 'JosefinSans-Bold',
                            marginBottom: hp(0.5),
                          }}>
                          {data.nama}
                        </Text>
                      </View>
                      <View style={{flex: 0.5}}>
                        <TouchableOpacity onPress={() => deleteUser(data)}>
                          <FontAweSome
                            name="trash"
                            color={colors.primarydark}
                            size={hp(3.2)}
                          />
                        </TouchableOpacity>
                      </View>
                    </LinearGradient>
                  </Card>
                </TouchableOpacity>
              ))}
            </View>
            <View style={{height: hp(10)}} />
          </Content>
        )}
        <SideBar
          name={state.nama}
          isVisible={state.visibleSideBar}
          onBackButtonPress={() => toggleVisibleSideBar()}
        />
        {/* <ReLoadingModal visible={state.loading} title="Menyimpan Tes.." /> */}
      </ImageBackground>
    </Container>
  );
};

export default DataTes;

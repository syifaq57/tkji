/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {Container, Content, Card} from 'native-base';
import firestore from '@react-native-firebase/firestore';
import DeafaultHeader from '../../component/DeafaultHeader';
import ReLoading from '../../component/ReLoading';
import ReLoadingModal from '../../component/ReLoadingModal';
import SideBar from '../../component/SideBar';

import colors from '../../res/colors';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';

import {useNavigation} from '@react-navigation/native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const initialState = {
  loading: false,
  visibleSideBar: false,
  listType: 'user',
};

const DataUser = () => {
  const navigation = useNavigation();
  const [state, setState] = useState(initialState);

  const [userList, setUserList] = useState([]);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const toggleVisibleSideBar = () => {
    updateState({visibleSideBar: !state.visibleSideBar});
  };

  const userCollection = firestore().collection('user');
  const jurusanCollection = firestore().collection('jurusan');
  const kelasCollection = firestore().collection('kelas');
  const siswaCollection = firestore().collection('siswa');


  const loadTes = async () => {
    await updateState({loading: true});
    await userCollection.orderBy('createAt', 'desc').onSnapshot((snapshot) => {
      let list = [];
      snapshot.forEach((doc) => {
        list.push({
          id: doc.id,
          foto: doc.data().foto,
          nama: doc.data().nama,
          username: doc.data().username,
          password: doc.data().password,
          role: doc.data().role,
          createAt: doc.data().createAt,
        });
      });
      setUserList(list);
    });
    updateState({loading: false});
  };

  const loadJurusan = async () => {
    await updateState({loading: true});
    await jurusanCollection
      .orderBy('createAt', 'desc')
      .onSnapshot((snapshot) => {
        let list = [];
        snapshot.forEach((doc) => {
          list.push({
            id: doc.id,
            nama: doc.data().nama,
            createAt: doc.data().createAt,
          });
        });
        setUserList(list);
      });
    updateState({loading: false});
  };

  const loadKelas = async () => {
    await updateState({loading: true});
    await kelasCollection.orderBy('createAt', 'desc').onSnapshot((snapshot) => {
      let list = [];
      snapshot.forEach((doc) => {
        list.push({
          id: doc.id,
          nama: doc.data().nama,
          createAt: doc.data().createAt,
        });
      });
      setUserList(list);
    });
    updateState({loading: false});
  };

  const loadSiswa = async () => {
    await updateState({loading: true});
    await siswaCollection.orderBy('createAt', 'desc').onSnapshot((snapshot) => {
      let list = [];
      snapshot.forEach((doc) => {
        list.push({
          id: doc.id,
          nama: doc.data().nama,
          npm: doc.data().npm,
          kelas: doc.data().kelas,
          jurusan: doc.data().jurusan,
          createAt: doc.data().createAt,
        });
      });
      setUserList(list);
    });
    updateState({loading: false});
  };

  const onOpenEditUser = (data) => {
    if (state.listType === 'user') {
      navigation.navigate('InputUser', {user: data});
    } else if (state.listType === 'siswa') {
      navigation.navigate('InputSiswa', { siswa: data });
    } else if (state.listType === 'jurusan') {
      navigation.navigate('InputJurusan', {jurusan: data});
    } else {
      navigation.navigate('InputKelas', {kelas: data});
    }
  };

  const deleteUser = (data) => {
    userCollection.doc(data.id).delete();
    // this.props.reload();
  };

  const deleteJurusan = (data) => {
    jurusanCollection.doc(data.id).delete();
  };
  const deleteKelas = (data) => {
    kelasCollection.doc(data.id).delete();
  };

  useEffect(() => {
    if (state.listType === 'user') {
      loadTes();
    } else if (state.listType === 'siswa') {
      loadSiswa();
    } else if (state.listType === 'jurusan') {
      loadJurusan();
    } else {
      loadKelas();
    }
  }, [state.listType]);

  const buttonList = [
    {
      id: 'user',
      label: 'USER',
    },
    {
      id: 'siswa',
      label: 'Siswa',
    },
    {
      id: 'kelas',
      label: 'KELAS',
    },
    {
      id: 'jurusan',
      label: 'JURUSAN',
    },
  ];

  const KelasContent = () => {
    return (
      <View>
        {userList.map((data, index) => (
          <TouchableOpacity key={index} onPress={() => onOpenEditUser(data)}>
            <Card
              style={{
                marginVertical: hp(2),
                borderRadius: wp(2),
                backgroundColor: colors.primary,
              }}>
              <LinearGradient
                start={{x: 0.0, y: 0.25}}
                end={{x: 0.5, y: 1.0}}
                colors={[colors.primary, colors.bgErm]}
                style={{
                  flexDirection: 'row',
                  borderRadius: wp(2),
                  height: hp(10),
                  alignItems: 'center',
                  paddingHorizontal: wp(3),
                }}>
                <View style={{flex: 1, marginRight: wp(1)}}>
                  <Image
                    source={require('../../res/image/class.png')}
                    style={{
                      height: hp(6),
                      width: hp(6),
                      borderRadius: hp(7),
                      resizeMode: 'cover',
                    }}
                  />
                </View>
                <View style={{flex: 6}}>
                  <Text
                    ellipsizeMode="tail"
                    numberOfLines={1}
                    style={{
                      color: colors.primarydark,
                      fontSize: wp(3.2),
                      fontFamily: 'JosefinSans-Bold',
                      marginBottom: hp(0.5),
                    }}>
                    {data.nama}
                  </Text>
                </View>
                <View style={{flex: 0.5}}>
                  <TouchableOpacity onPress={() => deleteKelas(data)}>
                    <FontAweSome
                      name="trash"
                      color={colors.primarydark}
                      size={hp(3.2)}
                    />
                  </TouchableOpacity>
                </View>
              </LinearGradient>
            </Card>
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  const JurusanContent = () => {
    return (
      <View>
        {userList.map((data, index) => (
          <TouchableOpacity key={index} onPress={() => onOpenEditUser(data)}>
            <Card
              style={{
                marginVertical: hp(2),
                borderRadius: wp(2),
                backgroundColor: colors.primary,
              }}>
              <LinearGradient
                start={{x: 0.0, y: 0.25}}
                end={{x: 0.5, y: 1.0}}
                colors={[colors.primary, colors.bgErm]}
                style={{
                  flexDirection: 'row',
                  borderRadius: wp(2),
                  height: hp(10),
                  alignItems: 'center',
                  paddingHorizontal: wp(3),
                }}>
                <View style={{flex: 1, marginRight: wp(1)}}>
                  <Image
                    source={require('../../res/image/jurusan.png')}
                    style={{
                      height: hp(6),
                      width: hp(6),
                      borderRadius: hp(7),
                      resizeMode: 'cover',
                    }}
                  />
                </View>
                <View style={{flex: 6}}>
                  <Text
                    ellipsizeMode="tail"
                    numberOfLines={1}
                    style={{
                      color: colors.primarydark,
                      fontSize: wp(3.2),
                      fontFamily: 'JosefinSans-Bold',
                      marginBottom: hp(0.5),
                    }}>
                    {data.nama}
                  </Text>
                </View>
                <View style={{flex: 0.5}}>
                  <TouchableOpacity onPress={() => deleteJurusan(data)}>
                    <FontAweSome
                      name="trash"
                      color={colors.primarydark}
                      size={hp(3.2)}
                    />
                  </TouchableOpacity>
                </View>
              </LinearGradient>
            </Card>
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  const UserContent = () => {
    return (
      <View>
        {userList.map((data, index) => (
          <TouchableOpacity key={index} onPress={() => onOpenEditUser(data)}>
            <Card
              style={{
                marginVertical: hp(2),
                borderRadius: wp(2),
                backgroundColor: colors.primary,
              }}>
              <LinearGradient
                start={{x: 0.0, y: 0.25}}
                end={{x: 0.5, y: 1.0}}
                colors={[colors.primary, colors.bgErm]}
                style={{
                  flexDirection: 'row',
                  borderRadius: wp(2),
                  height: hp(10),
                  alignItems: 'center',
                  paddingHorizontal: wp(3),
                }}>
                <View style={{flex: 1, marginRight: wp(1)}}>
                  <Image
                    source={
                      data.foto
                        ? {
                            uri: data.foto,
                          }
                        : require('../../res/image/Profile.png')
                    }
                    style={{
                      height: hp(6),
                      width: hp(6),
                      borderRadius: hp(7),
                      resizeMode: 'cover',
                    }}
                  />
                </View>
                <View style={{flex: 6}}>
                  <Text
                    ellipsizeMode="tail"
                    numberOfLines={1}
                    style={{
                      color: colors.primarydark,
                      fontSize: wp(3.2),
                      fontFamily: 'JosefinSans-Bold',
                      marginBottom: hp(0.5),
                    }}>
                    {data.nama}
                  </Text>
                  <Text
                    style={{
                      color: colors.gray08,
                      fontSize: wp(2.5),
                      fontFamily: 'Poppins-Regular',
                    }}>
                    {data.role}
                  </Text>
                </View>
                <View style={{flex: 0.5}}>
                  <TouchableOpacity onPress={() => deleteUser(data)}>
                    <FontAweSome
                      name="trash"
                      color={colors.primarydark}
                      size={hp(3.2)}
                    />
                  </TouchableOpacity>
                </View>
              </LinearGradient>
            </Card>
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  const SiswaContent = () => {
    return (
      <View>
        {userList.map((data, index) => (
          <TouchableOpacity key={index} onPress={() => onOpenEditUser(data)}>
            <Card
              style={{
                marginVertical: hp(2),
                borderRadius: wp(2),
                backgroundColor: colors.primary,
              }}>
              <LinearGradient
                start={{x: 0.0, y: 0.25}}
                end={{x: 0.5, y: 1.0}}
                colors={[colors.primary, colors.bgErm]}
                style={{
                  flexDirection: 'row',
                  borderRadius: wp(2),
                  height: hp(10),
                  alignItems: 'center',
                  paddingHorizontal: wp(3),
                }}>
                <View style={{flex: 1, marginRight: wp(1)}}>
                  <Image
                    source={require('../../res/image/Profile.png')}
                    style={{
                      height: hp(6),
                      width: hp(6),
                      borderRadius: hp(7),
                      resizeMode: 'cover',
                    }}
                  />
                </View>
                <View style={{flex: 6}}>
                  <Text
                    ellipsizeMode="tail"
                    numberOfLines={1}
                    style={{
                      color: colors.primarydark,
                      fontSize: wp(3.2),
                      fontFamily: 'JosefinSans-Bold',
                      marginBottom: hp(0.5),
                    }}>
                    {data.nama}
                  </Text>
                  <Text
                    style={{
                      color: colors.gray08,
                      fontSize: wp(2.5),
                      fontFamily: 'Poppins-Regular',
                    }}>
                    {data.npm}
                  </Text>
                </View>
                <View style={{flex: 0.5}}>
                  <TouchableOpacity onPress={() => deleteUser(data)}>
                    <FontAweSome
                      name="trash"
                      color={colors.primarydark}
                      size={hp(3.2)}
                    />
                  </TouchableOpacity>
                </View>
              </LinearGradient>
            </Card>
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  return (
    <Container>
      <ImageBackground
        style={{flex: 1, backgroundColor: 'white'}}
        source={require('../../res/image/background.jpg')}>
        <DeafaultHeader
          title="Data Master"
          menuBar
          onMenuBarPress={() => {
            toggleVisibleSideBar();
          }}
          rightButton="plus"
          onRightButtonPress={() => {
            if (state.listType === 'user') {
              navigation.navigate('InputUser');
            } else if (state.listType === 'siswa') {
              navigation.navigate('InputSiswa');
            } else if (state.listType === 'jurusan') {
              navigation.navigate('InputJurusan');
            } else {
              navigation.navigate('InputKelas');
            }
          }}
        />
        <View
          style={{
            paddingHorizontal: wp(4),
            marginTop: hp(1),
            flexDirection: 'row',
          }}>
          {buttonList.map((data, index) => (
            <TouchableOpacity
              onPress={() => {
                updateState({
                  listType: data.id,
                });
              }}
              key={index}
              style={{
                flex: 1,
                borderRadius: wp(1),
                marginHorizontal: wp(1),
                backgroundColor: colors.primarydark,
                paddingHorizontal: data.id === state.listType ? wp(1.3) : wp(2),
                paddingVertical: data.id === state.listType ? hp(1.2) : hp(1.5),
                alignItems: 'center',
                borderWidth: data.id === state.listType ? 3 : 0,
                borderColor: colors.gray08,
              }}>
              <Text
                style={{
                  color: colors.white,
                  fontSize: wp(3),
                  fontFamily: 'JosefinSans-Bold',
                }}>
                {data.label}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
        {state.loading ? (
          <ReLoading />
        ) : (
          <Content style={{paddingHorizontal: wp(5), paddingVertical: hp(1.5)}}>
            {state.listType === 'user' ? (
              <UserContent />
            ) : state.listType === 'jurusan' ? (
              <JurusanContent />
            ) : state.listType === 'siswa' ? (
              <SiswaContent />
            ) : (
              <KelasContent />
            )}
            <View style={{height: hp(10)}} />
          </Content>
        )}
        <SideBar
          name={state.nama}
          isVisible={state.visibleSideBar}
          onBackButtonPress={() => toggleVisibleSideBar()}
        />
        {/* <ReLoadingModal visible={state.loading} title="Menyimpan Tes.." /> */}
      </ImageBackground>
    </Container>
  );
};

export default DataUser;

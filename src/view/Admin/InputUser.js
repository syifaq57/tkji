/* eslint-disable no-alert */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {Container, Content} from 'native-base';

import colors from '../../res/colors/index';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import uuid from 'react-native-uuid';

import DeafaultHeader from '../../component/DeafaultHeader';
import ReTextInput from '../../component/ReTextInput';
import ReTextArea from '../../component/ReTextArea';

import ReModalImagePicker from '../../component/ReModalImagePicker';
import ReButton from '../../component/ReButton';
import ReLoadingModal from '../../component/ReLoadingModal';

import {useNavigation, useRoute} from '@react-navigation/native';
import ReLoading from '../../component/ReLoading';
import RePicker from '../../component/RePicker';

const initialState = {
  loading: true,
  loadingModal: false,
  id: '',

  nama: '',
  username: '',
  password: '',
  role: '',
  foto: '',

  fotoObject: {},

  user: {},
  visiblePicker: false,
};

const roleList = [
  {value: 'User', label: 'User'},
  {value: 'Admin', label: 'Admin'},
];

const InputUser = () => {
  const route = useRoute();
  const navigation = useNavigation();

  const [state, setState] = useState(initialState);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const togglePicker = () => {
    updateState({visiblePicker: !state.visiblePicker});
  };

  const preparedFoto = async (foto) => {
    if (foto.length === 0) {
      console.log('jalaan');

      return '';
    } else {
      console.log('jalaan 2', foto);

      const uri = foto;
      const ext = uri.split('.').pop();
      const filename = `${uuid.v1()}.${ext}`;
      const imageRef = storage().ref(`foto/${filename}`);
      await imageRef.putFile(uri);
      const url = await imageRef.getDownloadURL();

      return url;
    }
  };

  const preparedData = async () => {
    const fotoUri =
      Object.keys(state.fotoObject).length > 0
        ? await preparedFoto(state.foto)
        : state.foto;

    return {
      nama: state.nama,
      username: state.username,
      password: state.password,
      role: state.role,
      foto: fotoUri,
      createAt: firestore.FieldValue.serverTimestamp(),
    };
  };

  const onSaveUser = async () => {
    await updateState({loadingModal: true});
    const newData = await preparedData();
    console.log('new data', newData);
    const userCollection = firestore().collection('user');

    await userCollection
      .add({
        nama: newData.nama,
        username: newData.username,
        password: newData.password,
        role: newData.role,
        foto: newData.foto,
        createAt: newData.createAt,
      })
      .then(() => {
        alert('Berhasil menyimpan data');
        setState(initialState);
        navigation.goBack();
      })
      .catch(() => {
        alert('Gagal menyimpan data');
      });
    await updateState({loadingModal: false});
  };

  const onUpdateUser = async () => {
    await updateState({loadingModal: true});

    const newData = await preparedData();
    console.log('new data', newData);
    const userCollection = firestore().collection('user');

    userCollection
      .doc(state.id)
      .update({
        nama: newData.nama,
        username: newData.username,
        password: newData.password,
        role: newData.role,
        foto: newData.foto,
        createAt: newData.createAt,
      })
      .then(() => {
        console.log('jalan update');
        alert('Berhasil menyimpan data');
        setState(initialState);
        navigation.goBack();
      })
      .catch(() => {
        alert('Gagal menyimpan data');
      });
    await updateState({loadingModal: false});
  };

  const checkParamsData = async () => {
    await updateState({loading: true});

    const user = (await route.params) ? route.params.user : null;
    if (user !== null) {
      await updateState({
        id: user.id,
        nama: user.nama,
        username: user.username,
        password: user.password,
        role: user.role,
        foto: user.foto,
        createAt: user.createAt,
      });
    }
    await updateState({loading: false});
  };

  useEffect(() => {
    checkParamsData();
  }, []);

  const checkDisableSave = () => {
    if (
      state.username.length > 0 &&
      state.password.length > 0 &&
      state.role.length > 0
    ) {
      return false;
    }
    return true;
  };

  return (
    <Container>
      <ImageBackground
        style={{flex: 1, backgroundColor: 'white'}}
        source={require('../../res/image/background.jpg')}>
        <DeafaultHeader
          title="Input Data User"
          backButton
          // rightButton={state.user.role === 'admin'}
          // onMenuBarPress={() => {
          //   toggleVisibleSideBar();
          // }}
        />
        {state.loading ? (
          <ReLoading />
        ) : (
          <Content style={{paddingHorizontal: wp(7), paddingVertical: hp(1.5)}}>
            <View style={{}}>
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginBottom: hp(2.4),
                }}>
                <TouchableOpacity
                  onPress={() => {
                    togglePicker();
                  }}>
                  {state.foto.length > 0 ? (
                    <Image
                      source={{
                        uri: state.foto,
                      }}
                      style={{
                        height: hp(12),
                        width: hp(12),
                        borderRadius: hp(22),
                        resizeMode: 'cover',
                      }}
                    />
                  ) : (
                    <Image
                      source={require('../../res/image/Profile.png')}
                      style={{
                        height: hp(12),
                        width: hp(12),
                        borderRadius: hp(15),
                        resizeMode: 'cover',
                      }}
                    />
                  )}
                </TouchableOpacity>
                <Text
                  style={{
                    fontSize: wp(4),
                    // fontWeight: 'bold',
                    marginTop: hp(1.5),
                  }}>
                  Tambah Foto Profile
                </Text>
              </View>
              <View style={{marginBottom: hp(1), marginTop: hp(2)}}>
                <ReTextInput
                  label="Nama"
                  value={state.nama}
                  onChangeText={(val) => {
                    updateState({nama: val});
                  }}
                  placeholder="Input nama"
                  labelColor={colors.primarydark}
                />
              </View>
              <View style={{marginVertical: hp(1)}}>
                <RePicker
                  label="Pilih Role"
                  items={roleList}
                  selectedValue={state.role}
                  noAnimation
                  onValueChange={(value) => {
                    updateState({
                      role: value,
                    });
                  }}
                />
              </View>

              <View style={{marginVertical: hp(1)}}>
                <ReTextInput
                  label="Username"
                  value={state.username}
                  onChangeText={(val) => {
                    updateState({username: val});
                  }}
                  placeholder="Input username"
                  labelColor={colors.primarydark}
                />
              </View>
              <View style={{marginVertical: hp(1)}}>
                <ReTextInput
                  label="Password"
                  value={state.password}
                  onChangeText={(val) => {
                    updateState({password: val});
                  }}
                  placeholder="Input password"
                  labelColor={colors.primarydark}
                  passwordInput
                />
              </View>
              <View style={{alignItems: 'center', marginTop: hp(5)}}>
                <View style={{width: wp(80)}}>
                  <ReButton
                    label="Simpan"
                    disabled={checkDisableSave()}
                    onPress={() => {
                      if (state.id) {
                        onUpdateUser(state.id);
                      } else {
                        onSaveUser();
                      }
                    }}
                  />
                </View>
              </View>
            </View>
            <View style={{height: hp(10)}} />
            <ReLoadingModal
              visible={state.loadingModal}
              title="Menyimpan Tes.."
            />
          </Content>
        )}

        <ReModalImagePicker
          visible={state.visiblePicker}
          onCancel={() => togglePicker()}
          onPickImage={(image) => {
            console.log(image);
            togglePicker();
            updateState({
              fotoObject: image,
              foto: image.uri,
            });
          }}
        />
      </ImageBackground>
    </Container>
  );
};

export default InputUser;

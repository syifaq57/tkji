/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {View, Text, ImageBackground, BackHandler, Alert} from 'react-native';

import {Container, Content} from 'native-base';

import AsyncStorage from '@react-native-async-storage/async-storage';

import {useNavigation, useFocusEffect} from '@react-navigation/native';

import colors from '../../res/colors/index';
import DeafaultHeader from '../../component/DeafaultHeader';
import SideBar from '../../component/SideBar';
import TestList from '../Component/TestList';

import firestore from '@react-native-firebase/firestore';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ReLoading from '../../component/ReLoading';

const HomeAdmin = () => {
  const [state, setState] = useState({
    visibleSideBar: false,
    loading: true,

    user: null,
  });

  const [tesList, setList] = useState([]);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const toggleVisibleSideBar = () => {
    updateState({visibleSideBar: !state.visibleSideBar});
  };

  const loadTes = async () => {
    const tesCollection = await firestore().collection('tes');
    await tesCollection.orderBy('createAt', 'desc').onSnapshot((snapshot) => {
      let list = [];
      snapshot.forEach((doc) => {
        list.push({
          id: doc.id,
          foto: doc.data().foto,
          nama: doc.data().nama,
          deskripsi: doc.data().deskripsi,
          createAt: doc.data().createAt,
        });
      });
      setList(list);
    });
  };

  const preparedScreen = async () => {
    await updateState({loading: true});

    await AsyncStorage.getItem('userData').then((item) => {
      const user = JSON.parse(item);
      if (user && Object.keys(user).length > 0) {
        updateState({
          user: user,
        });
      }
    });
    await loadTes();
    updateState({loading: false});
  };

  useEffect(() => {
    preparedScreen();
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        Alert.alert('Exit App!', 'Apakah Kamu yakin ingin keluar Aplikasi?', [
          {
            text: 'Cancel',
            onPress: () => {
              // return true;
            },
            style: 'cancel',
          },
          {
            text: 'YES',
            onPress: () => {
              BackHandler.exitApp();
              // return false;
            },
          },
        ]);
        return true;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, []),
  );

  return (
    <Container>
      <ImageBackground
        style={{flex: 1, backgroundColor: 'white'}}
        source={require('../../res/image/background.jpg')}>
        <DeafaultHeader
          title="DASHBOARD"
          menuBar
          onMenuBarPress={() => {
            toggleVisibleSideBar();
          }}
        />
        {state.loading ? (
          <ReLoading />
        ) : (
          <Content style={{paddingHorizontal: wp(5), paddingVertical: hp(1.5)}}>
            <View style={{marginLeft: wp(2.2), marginBottom: wp(1)}}>
              <Text
                style={{
                  fontSize: hp(2.2),
                  color: colors.gray08,
                  fontFamily: 'Poppins-SemiBold',
                }}>
                Selamat Datang di E-TKJI
              </Text>
              <Text
                style={{
                  fontSize: hp(2.2),
                  color: colors.gray10,
                  fontFamily: 'Poppins-Regular',
                }}>
                Tes Kesehatan Jasmani Indonesia
              </Text>
            </View>
            <TestList
              loading={state.loading}
              data={tesList}
              role={state.user?.role}
            />
            <View style={{height: hp(10)}} />
          </Content>
        )}
        <SideBar
          user={state.user}
          isVisible={state.visibleSideBar}
          onBackButtonPress={() => toggleVisibleSideBar()}
        />
      </ImageBackground>
    </Container>
  );
};

export default HomeAdmin;

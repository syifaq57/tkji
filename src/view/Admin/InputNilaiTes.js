/* eslint-disable no-alert */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {View, Text, ImageBackground, Alert} from 'react-native';
import {Container, Content, Card} from 'native-base';

import colors from '../../res/colors/index';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import firestore from '@react-native-firebase/firestore';

import DeafaultHeader from '../../component/DeafaultHeader';
import ReTextInput from '../../component/ReTextInput';

import ReModalImagePicker from '../../component/ReModalImagePicker';
import ReButton from '../../component/ReButton';
import ReLoadingModal from '../../component/ReLoadingModal';

import {useNavigation, useRoute} from '@react-navigation/native';
import ReLoading from '../../component/ReLoading';
import RePicker from '../../component/RePicker';
import RePickerCustom from '../../component/RePickerCustom';

const initialState = {
  loading: true,
  id: '',

  siswa: null,
  nilai: '',

  tanggal: '',
  periode: '',
  namaTes: '',
  idTes: '',

  siswaList: [],

  tesFilled: false,
  showDelete: false,
};

const InputNilaiTes = () => {
  const route = useRoute();
  const navigation = useNavigation();

  const [state, setState] = useState(initialState);
  const [loading, setLoading] = useState(false);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const togglePicker = () => {
    updateState({visiblePicker: !state.visiblePicker});
  };

  const preparedData = async () => {
    return {
      siswaId: state.siswa.id,
      nilai: state.nilai,
      tanggal: state.tanggal,
      namaTes: state.namaTes,
      idTes: state.idTes,
      periode: state.periode,
      createAt: firestore.FieldValue.serverTimestamp(),
    };
  };

  const preparedSiswa = async (tesdata) => {
    const tesList =
      state.siswa.tesList && state.siswa.tesList.length > 0
        ? state.siswa.tesList
        : [];

    tesList.push(firestore().doc('tesList/' + tesdata));

    return {
      nama: state.siswa.nama,
      npm: state.siswa.npm,
      kelas: state.siswa.kelas,
      jurusan: state.siswa.jurusan,
      createAt: firestore.FieldValue.serverTimestamp(),
      tesList: tesList,
    };
  };

  const onUpdateUser = async (tesData) => {
    const newData = await preparedSiswa(tesData);
    // console.log('new data', newData);
    const userCollection = firestore().collection('siswa');

    userCollection
      .doc(state.siswa.id)
      .update({
        ...newData,
      })
      .then(() => {
        // console.log('jalan update');
        // alert('Berhasil menyimpan data');
        // setState(initialState);
        // navigation.goBack();
      })
      .catch(() => {
        alert('Gagal upadte siswa');
      });
  };

  const tesCollection = firestore().collection('tesList');

  const onSaveTes = async () => {
    await setLoading(true);
    const newData = await preparedData();
    console.log('new data', state.siswa);

    await tesCollection
      .add({
        ...newData,
      })
      .then((doc) => {
        console.log('doc. id', doc);
        onUpdateUser(doc.id);
        alert('Berhasil menyimpan data');
        setState(initialState);
        navigation.goBack();
      })
      .catch(() => {
        alert('Gagal menyimpan data');
      });
    await setLoading(false);
  };

  const onUpdateTes = async (docId) => {
    await setLoading(true);
    const newData = await preparedData();
    console.log('new data', newData);

    await tesCollection
      .doc(docId)
      .update({...newData})
      .then(() => {
        alert('Berhasil menyimpan data');
        setState(initialState);
        navigation.goBack();
      })
      .catch(() => {
        alert('Gagal menyimpan data');
      });
    await setLoading(false);
  };

  const deleteTes = async () => {
    let dataTes = await firestore().collection('tesList');
    dataTes.doc(state.id).delete();
    navigation.goBack();
    // loadTes();
    // this.props.reload();
  };

  const checkParamsData = async () => {
    const tesDetail = (await route.params) ? await route.params.namaTes : null;

    const isEdit = (await route.params) ? await route.params.edited : null;

    const tesData = (await route.params) ? await route.params.detailTes : null;

    // console.log('test detail', tesData);
    if (isEdit) {
      console.log('iseEdit', isEdit);
      updateState({showDelete: true});
    }
    if (tesData) {
      updateState({
        id: tesData.id,
        siswa: tesData.siswaId,
        nilai: tesData.nilai,
        tanggal: tesData.periode,
      });
    }
    if (tesDetail !== null) {
      await updateState({
        namaTes: tesDetail.nama,
        idTes: tesDetail.id,
      });
    }
  };

  const getDateNow = () => {
    var currentdate = new Date();
    let start = new Date(currentdate.toLocaleDateString());
    return start.getTime();
  };

  const normalizeDate = (date) => {
    const year = new Date(date).getFullYear();

    const mont = new Date(date).getMonth();

    const day = new Date(date).getDate();

    const currentDate = day + '/' + mont + '/' + year;

    return currentDate;
  };

  const isAlreadyTes = () => {
    return true;
  };

  const onReviewTes = async () => {
    let list = [];
    // isAlreadyTes();
    await tesCollection
      .where('siswaId', '==', state.siswa.id)
      .where('idTes', '==', state.idTes)
      .where('periode', '==', state.periode)
      .get()
      .then((snap) => {
        snap.forEach((doc) => {
          list.push({
            ...doc.data(),
          });
        });
      });
    if (list.length > 0) {
      Alert.alert(
        'Data Tes Tidak Sesuai',
        `${state.siswa.nama} sudah mengikuti Tes ini di periode ${list[0]?.periode}`,
      );
    } else {
      updateState({tesFilled: !state.tesFilled});
    }
    console.log('tes a', list);
  };

  const checkFormFilled = () => {
    if (
      state.siswa &&
      Object.keys(state.siswa).length > 0 &&
      state.nilai.length > 0
    ) {
      return false;
    }
    return true;
  };

  const loadSiswa = async () => {
    const siswa = firestore().collection('siswa');

    const siswas = await siswa.get().then((querySnapshot) => {
      let list = [];
      querySnapshot.forEach((doc) => {
        list.push({
          id: doc.id,
          ...doc.data(),
        });
      });
      return list;
    });

    let siswaSelected = null;

    const tesData = (await route.params) ? await route.params.detailTes : null;

    if (tesData) {
      siswaSelected = await siswas.find((ssw) => ssw.id === tesData.siswaId);
    }

    console.log('siswa sel', siswaSelected)

    await updateState({siswaList: siswas, siswa: siswaSelected});
  };

  const preparedScreen = async () => {
    await updateState({loading: true});
    await updateState({
      tanggal: getDateNow(),
      periode: new Date().getFullYear().toString(),
    });

    await checkParamsData();

    await loadSiswa();

    await updateState({loading: false});
  };

  useEffect(() => {
    preparedScreen();
  }, []);

  const FormReview = () => {
    return (
      <View style={{marginLeft: wp(2.2), marginVertical: wp(2.5)}}>
        <Card
          style={{
            borderRadius: wp(2),
            paddingVertical: hp(2),
            paddingHorizontal: wp(3),
          }}>
          <View style={{marginBottom: hp(1)}}>
            <Text
              style={{
                fontSize: wp(3),
                fontFamily: 'JosefinSans-Medium',
                color: colors.gray11,
              }}>
              Nama
            </Text>
            <Text
              style={{
                fontSize: wp(4),
                fontFamily: 'JosefinSans-Bold',
                color: colors.gray08,
              }}>
              {state.siswa?.nama}
            </Text>
          </View>
          <View style={{marginBottom: hp(1)}}>
            <Text
              style={{
                fontSize: wp(3),
                fontFamily: 'JosefinSans-Medium',
                color: colors.gray11,
              }}>
              Kelas
            </Text>
            <Text
              style={{
                fontSize: wp(4),
                fontFamily: 'JosefinSans-Bold',
                color: colors.gray08,
              }}>
              {state.siswa?.kelas}
            </Text>
          </View>
          <View style={{marginBottom: hp(1)}}>
            <Text
              style={{
                fontSize: wp(3),
                fontFamily: 'JosefinSans-Medium',
                color: colors.gray11,
              }}>
              Jurusan
            </Text>
            <Text
              style={{
                fontSize: wp(4),
                fontFamily: 'JosefinSans-Bold',
                color: colors.gray08,
              }}>
              {state.siswa?.jurusan}
            </Text>
          </View>
          <View style={{marginBottom: hp(1)}}>
            <Text
              style={{
                fontSize: wp(3),
                fontFamily: 'JosefinSans-Medium',
                color: colors.gray11,
              }}>
              Hasil Nilai
            </Text>
            <Text
              style={{
                fontSize: wp(4),
                fontFamily: 'JosefinSans-Bold',
                color: colors.gray08,
              }}>
              {state.nilai}
            </Text>
          </View>
          <View style={{marginBottom: hp(1)}}>
            <Text
              style={{
                fontSize: wp(3),
                fontFamily: 'JosefinSans-Medium',
                color: colors.gray11,
              }}>
              Jenis Tes
            </Text>
            <Text
              style={{
                fontSize: wp(4),
                fontFamily: 'JosefinSans-Bold',
                color: colors.gray08,
              }}>
              {state.namaTes}
            </Text>
          </View>
          <View style={{marginBottom: hp(1)}}>
            <Text
              style={{
                fontSize: wp(3),
                fontFamily: 'JosefinSans-Medium',
                color: colors.gray11,
              }}>
              Tanggal Tes / Periode
            </Text>
            <Text
              style={{
                fontSize: wp(4),
                fontFamily: 'JosefinSans-Bold',
                color: colors.gray08,
              }}>
              {normalizeDate(state.tanggal)}
            </Text>
          </View>
        </Card>
        <View
          style={{
            marginVertical: wp(4),
            flexDirection: 'row',
            justifyContent: 'center',
          }}>
          <View style={{width: wp(40)}}>
            <ReButton
              label="Batal"
              onPress={() => {
                updateState({tesFilled: false});
              }}
            />
          </View>
          <View style={{width: wp(5)}} />
          <View style={{width: wp(40)}}>
            <ReButton
              label="Simpan"
              onPress={() => {
                if (state.id) {
                  onUpdateTes(state.id);
                } else {
                  onSaveTes();
                }
              }}
            />
          </View>
        </View>
      </View>
    );
  };

  return (
    <Container>
      <ImageBackground
        style={{flex: 1, backgroundColor: 'white'}}
        source={require('../../res/image/background.jpg')}>
        <DeafaultHeader
          title="Buat Penilaian Tes"
          backButton
          rightButton={state.showDelete > 0 ? 'trash' : false}
          onRightButtonPress={() => {
            deleteTes();
          }}
        />
        {state.loading ? (
          <ReLoading />
        ) : (
          <Content style={{paddingHorizontal: wp(5), paddingVertical: hp(1)}}>
            {state.tesFilled ? (
              <FormReview />
            ) : (
              <View style={{marginHorizontal: wp(2), marginVertical: wp(2.5)}}>
                <View style={{marginVertical: hp(1)}}>
                  <ReTextInput
                    label="Nama Tes"
                    value={state.namaTes}
                    placeholder="Input nama"
                    labelColor={colors.primarydark}
                    editable={false}
                  />
                </View>

                <View>
                  <RePickerCustom
                    label="Pilih Siswa"
                    value={state.siswa}
                    onValueChange={(val) => {
                      console.log('siswa', val);
                      updateState({siswa: val});
                    }}
                    data={state.siswaList}
                    displayValue="nama"
                    prefixValue="npm"
                  />
                </View>
                <View style={{marginVertical: hp(1)}}>
                  <ReTextInput
                    label="Hasil Nilai"
                    value={state.nilai}
                    onChangeText={(val) => {
                      updateState({nilai: val});
                    }}
                    placeholder="Input Hasil Nilai"
                    labelColor={colors.primarydark}
                    numeric
                  />
                </View>
                <View style={{marginVertical: wp(4), alignItems: 'center'}}>
                  <View style={{width: wp(80)}}>
                    <ReButton
                      label="Tinjau"
                      disabled={checkFormFilled()}
                      onPress={() => {
                        onReviewTes();
                      }}
                    />
                  </View>
                </View>
              </View>
            )}
            <View style={{height: hp(10)}} />
          </Content>
        )}

        <ReLoadingModal visible={loading} title="Menyimpan Tes.." />

        <ReModalImagePicker
          visible={state.visiblePicker}
          onCancel={() => togglePicker()}
          onPickImage={(image) => {
            console.log(image);
            togglePicker();
            updateState({
              fotoObject: image,
              foto: image.uri,
            });
          }}
        />
      </ImageBackground>
    </Container>
  );
};

export default InputNilaiTes;

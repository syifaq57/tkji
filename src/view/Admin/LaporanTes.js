/* eslint-disable no-const-assign */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  Alert,
  PermissionsAndroid,
  ActivityIndicator,
} from 'react-native';
import {Container, Content} from 'native-base';
import firestore from '@react-native-firebase/firestore';
import DeafaultHeader from '../../component/DeafaultHeader';
import ReLoading from '../../component/ReLoading';
import ReLoadingModal from '../../component/ReLoadingModal';
import SideBar from '../../component/SideBar';
import ReDatePicker from '../../component/ReDatePicker';
import ReButton from '../../component/ReButton';

import colors from '../../res/colors';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import ReModal from 'react-native-modal';

import {useNavigation, useIsFocused} from '@react-navigation/native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import RNFS, {writeFile, stat} from 'react-native-fs';

import FileViewer from 'react-native-file-viewer';

import XLSX from 'xlsx';
import RePicker from '../../component/RePicker';

const initialState = {
  loading: false,
  loadingModal: false,
  visibleSideBar: false,
  toggleFilter: false,

  date: '',
  tesSelected: {},
  listTes: [],

  tes: [],
  year: 2021,
};

const initialTable = {
  tableHead: [],
  tableData: [],
};

const initialFilter = {
  typeTes: '',
  jurusan: '',
  kelas: '',
  date: '',

  listJurusan: [],
  listKelas: [],
  listTes: [],
};

const LaporanTes = () => {
  const navigation = useNavigation();

  const isFocused = useIsFocused();

  const [state, setState] = useState(initialState);

  const [table, setTable] = useState(initialTable);

  const [siswa, setSiswa] = useState(null);

  const [filter, setFilter] = useState(initialFilter);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const updateFilter = (newData) => {
    setFilter((prev) => ({...prev, ...newData}));
  };

  const toggleVisibleSideBar = () => {
    updateState({visibleSideBar: !state.visibleSideBar});
  };

  const normalizeDate = (date) => {
    const year = new Date(date).getFullYear();

    const mont = new Date(date).getMonth();

    const day = new Date(date).getDate();

    const currentDate = day + '/' + mont + '/' + year;

    return currentDate;
  };

  const dateToTime = (date) => {
    var currentdate = new Date(date);
    let start = new Date(currentdate.toLocaleDateString());
    return start.getTime();
  };

  const exportExcel = async (data, date) => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Akses Dokumen',
          message: 'Aplikasi membutuhkan akses untuk menyimpan file',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // console.log('You can use the camera');
        const ws = await XLSX.utils.json_to_sheet(data);
        const wb = await XLSX.utils.book_new();

        await XLSX.utils.book_append_sheet(wb, ws, 'SheetJS');

        ws['!cols'] = [
          {width: 10},
          {width: 20},
          {width: 25},
          {width: 20},
          {width: 20},
          {width: 20},
          {width: 30},
        ];
        // ws['!rows'] = [{}];

        const wbout = await XLSX.write(wb, {type: 'binary', bookType: 'xlsx'});
        const file = RNFS.DownloadDirectoryPath + '/' + `report_${date}.xlsx`;
        await writeFile(file, wbout, 'ascii')
          .then(async () => {
            Alert.alert('Export Excel', `File telah di export ke ${file}?`, [
              {
                text: 'Tutup',
                onPress: () => {
                  // return true;
                },
                style: 'cancel',
              },
              {
                text: 'Buka File',
                onPress: () => {
                  FileViewer.open(file);
                },
              },
            ]);
          })
          .catch((err) => {
            Alert.alert('exportFile Error', 'Error ' + err.message);
          });
      } else {
        console.log('permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
    /* convert AOA back to worksheet */
  };

  const onPreparedExport = async () => {
    const typeTes = await loadTypeTes();

    const dataExport = await table.tableData.map((data, index) => {
      let list = {};
      typeTes.map((item) => {
        Object.assign(list, {[item.nama]: data[item.nama] || 0});
      });
      // console.log('list', list);
      //  const newList = await Promise.all(list)

      return {
        'No Ujian': index + 1,
        'No Pendaftaran': data.npm,
        Nama: data.nama,
        ...list,
        Total: data.total,
      };
    });

    // const tanggal =
    //   filter.date.length > 0 ? dateToTime(filter.date) : 'ALL_DATA';

    await exportExcel(dataExport, state.year.toString());
  };

  const getTableData = () => {
    let result = table.tableData;
    // const time = dateToTime(filter.date);

    if (filter.date.length > 0) {
      result = result.filter((v) => {
        return v.periode.toString().includes(dateToTime(filter.date));
      });
    }
    return result;
  };

  // useEffect(() => {
  //   loadTes();
  // }, [isFocused]);

  const loadTypeTes = () => {
    const tesCollect = firestore().collection('tes');
    const collect = tesCollect.get().then((qs) => {
      return qs.docs.map((doc) => {
        const data = doc.data();
        return data;
      });
    });

    return collect;
  };

  const rangeText = (nilai) => {
    nilai = parseInt(nilai);
    if (nilai <= 20) {
      return 'Sangat Kurang';
    } else if (nilai > 20 && nilai <= 40) {
      return 'Kurang';
    } else if (nilai > 40 && nilai <= 60) {
      return 'Cukup';
    } else if (nilai > 60 && nilai <= 80) {
      return 'Baik';
    } else if (nilai > 80 && nilai <= 100) {
      return 'Sangat Baik';
    } else {
      return '';
    }
  };

  const loadSiswa = async () => {
    await updateState({loading: true});
    const siswaCollection = firestore().collection('siswa');
    const collect = await siswaCollection
      .orderBy('createAt', 'desc')
      .get()
      .then((qs) => {
        return qs.docs.map(async (doc) => {
          const data = doc.data();
          data.total = 0;

          if (data.tesList) {
            const satu = data.tesList.map((tes) => {
              return tes.get();
            });
            const result = await Promise.all(satu);

            data.tesList = await result.map((t) => t.data());

            data.tesList = data.tesList.filter((ts) => {
              return ts.periode === state.year.toString();
            });

            data.tesList.map((da) => {
              // data[da.idTes] = da.nilai;
              data[da.namaTes] = da.nilai;
              data.total = `${parseInt(
                parseInt(data.total) + parseInt(da.nilai) / data.tesList.length,
              )} ${rangeText(
                parseInt(data.total) + parseInt(da.nilai) / data.tesList.length,
              )}`;
            });

            // total = ad data.teslit lengt
          } else {
            data.tesList = [];
          }
          return data;
        });
      });

    const promiseCollect = await Promise.all(collect);

    const tableH = [
      {id: 'npm', label: 'No Pendaftaran', width: 10},
      {id: 'nama', label: 'Nama', width: 10},
      // {id: 'kelas', label: 'Kelas', width: 10},
    ];
    const newHead = await loadTypeTes();
    newHead.map((item) => {
      tableH.push({
        id: item.nama,
        label: item.nama,
        width: 10,
        isTes: true,
      });
    });
    tableH.push({
      id: 'total',
      label: 'Total / Rata - Rata',
      width: 10,
      isTes: true,
    });

    setTable({tableHead: tableH, tableData: await promiseCollect});
    updateState({loading: false});
  };

  // useEffect(() => {
  //   loadSiswa();
  // }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      loadSiswa();
    });
    return unsubscribe;
  }, [navigation]);

  const getYear = () => {
    const years = [];
    const getCurrenYear = new Date().getFullYear();
    for (let i = 2021; i <= getCurrenYear; i++) {
      years.push({
        label: i.toString(),
        value: i,
      });
      // console.log('year', i);
    }

    return years;
  };

  return (
    <Container>
      <ImageBackground
        style={{flex: 1, backgroundColor: 'white'}}
        source={require('../../res/image/background.jpg')}>
        <DeafaultHeader
          title="Laporan & Hasil Tes"
          menuBar
          onMenuBarPress={() => {
            toggleVisibleSideBar();
          }}
          rightButton={
            Object.keys(state.tesSelected).length > 0 ? 'pencil' : false
          }
          onRightButtonPress={() => {
            // deleteTes();
            navigation.navigate('InputNilaiTes', {
              edited: true,
              detailTes: state.tesSelected,
              namaTes: {
                nama: state.tesSelected.namaTes,
                id: state.tesSelected.namaTes,
              },
            });
          }}
        />
        <View
          style={{
            paddingHorizontal: wp(5.2),
            marginVertical: hp(1),
            alignItems: 'flex-end',
            flexDirection: 'row',
          }}>
          <View style={{flex: 2, marginRight: wp(1)}}>
            {/* <ReDatePicker
              placeholder="Periode Tes"
              label="Periode Tes"
              selectedDate={filter.date}
              onDateChange={(date) => {
                updateFilter({date: date});
              }}
            /> */}

            <RePicker
              label="Pilih Periode"
              items={getYear()}
              selectedValue={state.year}
              noAnimation
              onValueChange={(value) => {
                console.log('vla', value);
                updateState({
                  year: value,
                });
              }}
            />
          </View>
          {/* <View style={{flex: 1, marginTop: hp(1), alignItems: 'flex-end'}}>
            <TouchableOpacity
              onPress={() => {
                resetReport();
              }}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: colors.primarydark,
                width: wp(9),
                height: wp(9),
                borderRadius: wp(1),
              }}>
              <FontAweSome name="undo" color={colors.white} size={hp(3)} />
            </TouchableOpacity>
          </View>
          <View style={{flex: 1, marginTop: hp(1), alignItems: 'center'}}>
            <TouchableOpacity
              onPress={() => toggleFilter()}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: colors.primarydark,
                width: wp(9),
                height: wp(9),
                borderRadius: wp(1),
              }}>
              <FontAweSome name="filter" color={colors.white} size={hp(3)} />
            </TouchableOpacity>
          </View> */}
          <View style={{flex: 2, marginTop: hp(1), alignItems: 'center'}}>
            <TouchableOpacity
              onPress={() => {
                onPreparedExport();
              }}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: colors.primarydark,
                width: '100%',
                height: wp(9),
                borderRadius: wp(1),
              }}>
              <Text
                style={{
                  fontSize: wp(3.2),
                  fontFamily: 'JosefinSans-Medium',
                  color: 'white',
                }}>
                EXPORT
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        {state.loading ? (
          <ReLoading />
        ) : (
          <Content style={{paddingHorizontal: wp(5), paddingVertical: hp(1.5)}}>
            <View>
              <ScrollView horizontal={true}>
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: colors.gray11,
                    backgroundColor: colors.primarydark,
                    flexDirection: 'row',
                  }}>
                  {table.tableHead.map((data, indexHeader) => (
                    <View key={indexHeader}>
                      <View
                        style={{
                          paddingVertical: hp(1),
                          paddingHorizontal: wp(2),
                          borderRightWidth:
                            indexHeader === table.tableHead.length - 1 ? 0 : 1,
                          borderBottomWidth: 1,
                          borderColor: colors.gray11,
                        }}>
                        <Text
                          style={{
                            fontSize: wp(3.2),
                            fontFamily: 'JosefinSans-Medium',
                            color: 'white',
                          }}>
                          {data.label}
                        </Text>
                      </View>
                      {getTableData().map((v, index) => {
                        const isSelected = () => {
                          if (data.isTes && v[data.id]) {
                            const find = v.tesList.find((i) => {
                              return i.namaTes === data.id;
                            });
                            if (find === state.tesSelected) {
                              return true;
                            } else {
                              return false;
                            }
                          }
                          return false;
                        };

                        return (
                          <TouchableOpacity
                            key={index}
                            onPress={() => {
                              if (data.isTes && v[data.id]) {
                                const tes = v.tesList.find((i) => {
                                  return i.namaTes === data.id;
                                });
                                console.log(tes)
                                updateState({tesSelected: tes});
                              } else {
                                updateState({tesSelected: {}});
                              }
                            }}>
                            <View
                              style={{
                                paddingHorizontal: wp(2),
                                backgroundColor: isSelected()
                                  ? colors.orangeDefault
                                  : !index % 2
                                  ? colors.lightGray
                                  : colors.gray01,
                                paddingVertical: hp(1),
                                borderRightWidth:
                                  indexHeader === table.tableHead.length - 1
                                    ? 0
                                    : 1,
                                borderColor: colors.gray11,
                              }}>
                              <View
                                style={
                                  data.isTes ? {alignItems: 'center'} : null
                                }>
                                <Text
                                  style={{
                                    fontSize: wp(3.2),
                                    fontFamily: 'JosefinSans-Regular',
                                    color: colors.gray08,
                                  }}>
                                  {v[data.id]}
                                </Text>
                              </View>
                            </View>
                          </TouchableOpacity>
                        );
                      })}
                    </View>
                  ))}
                </View>
              </ScrollView>
            </View>
            <View style={{height: hp(10)}} />
          </Content>
        )}
        <SideBar
          name={state.nama}
          isVisible={state.visibleSideBar}
          onBackButtonPress={() => toggleVisibleSideBar()}
        />
        <ReLoadingModal visible={state.loading} title="Loading.." />
      </ImageBackground>
    </Container>
  );
};

export default LaporanTes;

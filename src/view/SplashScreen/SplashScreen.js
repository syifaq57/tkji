/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';
import {StyleSheet, Alert, ActivityIndicator} from 'react-native';
import {Container, View, Text} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import AsyncStorage from '@react-native-async-storage/async-storage';

import firestore from '@react-native-firebase/firestore';

// const initialLogin = {
//   username: '',
//   password: '',
// };

const SplashScreen = ({navigation}) => {
  // const [loginForm, setLoginForm] = useState(initialLogin);

  const LoginUser = async (username, password) => {
    // await setLoading(true);
    let User;

    await firestore()
      .collection('user')
      .where('username', '==', username)
      .where('password', '==', password)
      .get()
      .then(async (snapshot) => {
        await snapshot.forEach((doc) => {
          if (doc) {
            User = {
              id: doc.id,
              username: doc.data().username,
              password: doc.data().password,
              nama: doc.data().nama,
              foto: doc.data().foto,
              role: doc.data().role,
            };
          }
        });
        console.log('user', User);
        if (User) {
          await AsyncStorage.setItem('userData', JSON.stringify(User));
          await navigation.navigate('HomeAdmin');
        } else {
          Alert.alert('Gagal Login', 'Username / Password tidak sesuai');
          await navigation.navigate('LoginScreen');
        }
        // setLoading(false);
      })
      .catch(() => {
        Alert.alert('Gagal Login', 'Check Koneksi Internet');
        // setLoading(false);
        navigation.navigate('LoginScreen');
      });
  };

  useEffect(() => {
    AsyncStorage.getItem('userData').then((item) => {
      const user = JSON.parse(item);
      if (user && Object.keys(user).length > 0) {
        LoginUser(user.username, user.password);
      } else {
        setTimeout(() => {
          navigation.navigate('LoginScreen');
        }, 3000);
      }
    });
  }, []);

  return (
    <Container>
      <View
        style={{
          backgroundColor: 'black',
          flex: 1,
          justifyContent: 'center',
          padding: 20,
        }}>
        <View style={stylePage.viewPT}>
          <View style={stylePage.logoView}>
            <Text style={{color: 'white', fontSize: wp(5), fontWeight: 'bold'}}>
              e-TKJI
            </Text>
          </View>
          <View style={stylePage.logoView}>
            <View style={{paddingVertical: hp('2%')}}>
              <ActivityIndicator size={'large'} color="white" />
            </View>
          </View>
          {/* <View style={stylePage.logoView}>
            <Text style={{fontSize: wp('4.5%'), fontWeight: 'bold'}}>
              Loading . . . .
            </Text>
          </View> */}
        </View>
      </View>
    </Container>
  );
};

const stylePage = StyleSheet.create({
  viewPT: {
    marginHorizontal: wp('5%'),
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -15,
  },
  logoView: {
    alignItems: 'center',
  },
  logo: {
    width: hp('20%'),
    height: hp('20%'),
    resizeMode: 'contain',
    // borderRadius: wp(30),
  },
  loading: {
    width: hp('10%'),
    height: hp('10%'),
    resizeMode: 'contain',
  },
});

export default SplashScreen;
